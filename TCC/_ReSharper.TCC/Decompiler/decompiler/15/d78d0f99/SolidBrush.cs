﻿// Type: System.Drawing.SolidBrush
// Assembly: System.Drawing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
// Assembly location: C:\Windows\Microsoft.NET\Framework\v4.0.30319\System.Drawing.dll

using System;
using System.Drawing.Internal;
using System.Runtime.InteropServices;

namespace System.Drawing
{
  /// <summary>
  /// Defines a brush of a single color. Brushes are used to fill graphics shapes, such as rectangles, ellipses, pies, polygons, and paths. This class cannot be inherited.
  /// </summary>
  /// <filterpriority>1</filterpriority>
  public sealed class SolidBrush : Brush, ISystemColorTracker
  {
    private Color color = Color.Empty;
    private bool immutable;

    /// <summary>
    /// Gets or sets the color of this <see cref="T:System.Drawing.SolidBrush"/> object.
    /// </summary>
    /// 
    /// <returns>
    /// A <see cref="T:System.Drawing.Color"/> structure that represents the color of this brush.
    /// </returns>
    /// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.SolidBrush.Color"/> property is set on an immutable <see cref="T:System.Drawing.SolidBrush"/>.</exception><filterpriority>1</filterpriority><PermissionSet><IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/></PermissionSet>
    public Color Color
    {
      get
      {
        if (this.color == Color.Empty)
        {
          int color = 0;
          int solidFillColor = SafeNativeMethods.Gdip.GdipGetSolidFillColor(new HandleRef((object) this, this.NativeBrush), out color);
          if (solidFillColor != 0)
            throw SafeNativeMethods.Gdip.StatusException(solidFillColor);
          this.color = Color.FromArgb(color);
        }
        return this.color;
      }
      set
      {
        if (this.immutable)
        {
          throw new ArgumentException(System.Drawing.SR.GetString("CantChangeImmutableObjects", new object[1]
          {
            (object) "Brush"
          }));
        }
        else
        {
          if (!(this.color != value))
            return;
          Color color = this.color;
          this.InternalSetColor(value);
          if (!value.IsSystemColor || color.IsSystemColor)
            return;
          SystemColorTracker.Add((ISystemColorTracker) this);
        }
      }
    }

    /// <summary>
    /// Initializes a new <see cref="T:System.Drawing.SolidBrush"/> object of the specified color.
    /// </summary>
    /// <param name="color">A <see cref="T:System.Drawing.Color"/> structure that represents the color of this brush. </param>
    public SolidBrush(Color color)
    {
      this.color = color;
      IntPtr brush = IntPtr.Zero;
      int solidFill = SafeNativeMethods.Gdip.GdipCreateSolidFill(this.color.ToArgb(), out brush);
      if (solidFill != 0)
        throw SafeNativeMethods.Gdip.StatusException(solidFill);
      this.SetNativeBrushInternal(brush);
      if (!color.IsSystemColor)
        return;
      SystemColorTracker.Add((ISystemColorTracker) this);
    }

    internal SolidBrush(Color color, bool immutable)
      : this(color)
    {
      this.immutable = immutable;
    }

    internal SolidBrush(IntPtr nativeBrush)
    {
      this.SetNativeBrushInternal(nativeBrush);
    }

    /// <summary>
    /// Creates an exact copy of this <see cref="T:System.Drawing.SolidBrush"/> object.
    /// </summary>
    /// 
    /// <returns>
    /// The <see cref="T:System.Drawing.SolidBrush"/> object that this method creates.
    /// </returns>
    /// <filterpriority>1</filterpriority><PermissionSet><IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/></PermissionSet>
    public override object Clone()
    {
      IntPtr clonebrush = IntPtr.Zero;
      int status = SafeNativeMethods.Gdip.GdipCloneBrush(new HandleRef((object) this, this.NativeBrush), out clonebrush);
      if (status != 0)
        throw SafeNativeMethods.Gdip.StatusException(status);
      else
        return (object) new SolidBrush(clonebrush);
    }

    protected override void Dispose(bool disposing)
    {
      if (!disposing)
        this.immutable = false;
      else if (this.immutable)
        throw new ArgumentException(System.Drawing.SR.GetString("CantChangeImmutableObjects", new object[1]
        {
          (object) "Brush"
        }));
      base.Dispose(disposing);
    }

    void ISystemColorTracker.OnSystemColorChanged()
    {
      if (!(this.NativeBrush != IntPtr.Zero))
        return;
      this.InternalSetColor(this.color);
    }

    private void InternalSetColor(Color value)
    {
      int status = SafeNativeMethods.Gdip.GdipSetSolidFillColor(new HandleRef((object) this, this.NativeBrush), value.ToArgb());
      if (status != 0)
        throw SafeNativeMethods.Gdip.StatusException(status);
      this.color = value;
    }
  }
}
