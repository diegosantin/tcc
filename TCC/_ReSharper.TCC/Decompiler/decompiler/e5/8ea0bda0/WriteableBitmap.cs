﻿// Type: System.Windows.Media.Imaging.WriteableBitmap
// Assembly: PresentationCore, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
// Assembly location: C:\Windows\Microsoft.NET\Framework\v4.0.30319\WPF\PresentationCore.dll

using MS.Internal;
using MS.Internal.PresentationCore;
using MS.Win32;
using MS.Win32.PresentationCore;
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Composition;

namespace System.Windows.Media.Imaging
{
  /// <summary>
  /// Provides a <see cref="T:System.Windows.Media.Imaging.BitmapSource"/> that can be written to and updated.
  /// </summary>
  public sealed class WriteableBitmap : BitmapSource
  {
    private bool _hasDirtyRects = true;
    private ManualResetEvent _copyCompletedEvent = new ManualResetEvent(true);
    [SecurityCritical]
    private IntPtr _backBuffer;
    [SecurityCritical]
    private uint _backBufferSize;
    private SecurityCriticalDataForSet<int> _backBufferStride;
    [SecurityCritical]
    private SafeMILHandle _pDoubleBufferedBitmap;
    [SecurityCritical]
    private SafeMILHandle _pBackBufferLock;
    [SecurityCritical]
    private BitmapSourceSafeMILHandle _pBackBuffer;
    private uint _lockCount;
    private bool _isWaitingForCommit;
    private EventHandler _committingBatchHandler;
    private bool _actLikeSimpleBitmap;

    /// <summary>
    /// Gets a pointer to the back buffer.
    /// </summary>
    /// 
    /// <returns>
    /// An <see cref="T:System.IntPtr"/> that points to the base address of the back buffer.
    /// </returns>
    public IntPtr BackBuffer
    {
      [SecurityCritical] get
      {
        SecurityHelper.DemandUnmanagedCode();
        this.ReadPreamble();
        return this._backBuffer;
      }
      [SecurityCritical] private set
      {
        this._backBuffer = value;
      }
    }

    /// <summary>
    /// Gets a value indicating the number of bytes in a single row of pixel data.
    /// </summary>
    /// 
    /// <returns>
    /// An integer indicating the number of bytes in a single row of pixel data.
    /// </returns>
    public int BackBufferStride
    {
      get
      {
        this.ReadPreamble();
        return this._backBufferStride.Value;
      }
    }

    EventHandler CommittingBatchHandler
    {
      private get
      {
        if (this._committingBatchHandler == null)
          this._committingBatchHandler = new EventHandler(this.OnCommittingBatch);
        return this._committingBatchHandler;
      }
    }

    internal WriteableBitmap()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="T:System.Windows.Media.Imaging.WriteableBitmap"/> class using the given <see cref="T:System.Windows.Media.Imaging.BitmapSource"/>.
    /// </summary>
    /// <param name="source">The <see cref="T:System.Windows.Media.Imaging.BitmapSource"/> to use for initialization.</param>
    [SecurityCritical]
    public WriteableBitmap(BitmapSource source)
      : base(true)
    {
      this.InitFromBitmapSource(source);
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="T:System.Windows.Media.Imaging.WriteableBitmap"/> class with the specified parameters.
    /// </summary>
    /// <param name="pixelWidth">The desired width of the bitmap.</param><param name="pixelHeight">The desired height of the bitmap.</param><param name="dpiX">The horizontal dots per inch (dpi) of the bitmap.</param><param name="dpiY">The vertical dots per inch (dpi) of the bitmap.</param><param name="pixelFormat">The <see cref="T:System.Windows.Media.PixelFormat"/> of the bitmap.</param><param name="palette">The <see cref="T:System.Windows.Media.Imaging.BitmapPalette"/> of the bitmap.</param>
    [SecurityCritical]
    public WriteableBitmap(int pixelWidth, int pixelHeight, double dpiX, double dpiY, PixelFormat pixelFormat, BitmapPalette palette)
      : base(true)
    {
      this.BeginInit();
      if (pixelFormat.Palettized && palette == null)
        throw new InvalidOperationException(MS.Internal.PresentationCore.SR.Get("Image_IndexedPixelFormatRequiresPalette"));
      if (pixelFormat.Format == PixelFormatEnum.Default)
        throw new ArgumentException(MS.Internal.PresentationCore.SR.Get("Effect_PixelFormat"), "pixelFormat");
      if (pixelWidth < 0)
        MS.Internal.HRESULT.Check(-2147024362);
      if (pixelWidth == 0)
        MS.Internal.HRESULT.Check(-2147024809);
      if (pixelHeight < 0)
        MS.Internal.HRESULT.Check(-2147024362);
      if (pixelHeight == 0)
        MS.Internal.HRESULT.Check(-2147024809);
      Guid guid = pixelFormat.Guid;
      SafeMILHandle pPalette = new SafeMILHandle();
      if (pixelFormat.Palettized)
        pPalette = palette.InternalPalette;
      MS.Internal.HRESULT.Check(MILSwDoubleBufferedBitmap.Create((uint) pixelWidth, (uint) pixelHeight, dpiX, dpiY, ref guid, pPalette, out this._pDoubleBufferedBitmap));
      this._pDoubleBufferedBitmap.UpdateEstimatedSize(this.GetEstimatedSize(pixelWidth, pixelHeight, pixelFormat));
      this.Lock();
      this.Unlock();
      this.EndInit();
    }

    /// <summary>
    /// Specifies the area of the bitmap that changed.
    /// </summary>
    /// <param name="dirtyRect">An <see cref="T:System.Windows.Int32Rect"/> representing the area that changed. Dimensions are in pixels. </param><exception cref="T:System.InvalidOperationException">The bitmap has not been locked by a call to the <see cref="M:System.Windows.Media.Imaging.WriteableBitmap.Lock"/> or <see cref="M:System.Windows.Media.Imaging.WriteableBitmap.TryLock(System.Windows.Duration)"/> methods. </exception><exception cref="T:System.ArgumentOutOfRangeException"><paramref name="dirtyRect"/> falls outside the bounds of the <see cref="T:System.Windows.Media.Imaging.WriteableBitmap"/>. </exception>
    [SecurityCritical]
    public void AddDirtyRect(Int32Rect dirtyRect)
    {
      this.WritePreamble();
      if ((int) this._lockCount == 0)
        throw new InvalidOperationException(MS.Internal.PresentationCore.SR.Get("Image_MustBeLocked"));
      dirtyRect.ValidateForDirtyRect("dirtyRect", this._pixelWidth, this._pixelHeight);
      if (!dirtyRect.HasArea)
        return;
      MILSwDoubleBufferedBitmap.AddDirtyRect(this._pDoubleBufferedBitmap, ref dirtyRect);
      this._hasDirtyRects = true;
    }

    /// <summary>
    /// Creates a modifiable clone of this <see cref="T:System.Windows.Media.Imaging.WriteableBitmap"/>, making deep copies of this object's values. When copying dependency properties, this method copies resource references and data bindings (but they might no longer resolve) but not animations or their current values.
    /// </summary>
    /// 
    /// <returns>
    /// A modifiable clone of the current object. The cloned object's <see cref="P:System.Windows.Freezable.IsFrozen"/> property will be false even if the source's <see cref="P:System.Windows.Freezable.IsFrozen"/> property was true.
    /// </returns>
    public WriteableBitmap Clone()
    {
      return (WriteableBitmap) base.Clone();
    }

    /// <summary>
    /// Creates a modifiable clone of this <see cref="T:System.Windows.Media.Animation.ByteAnimationUsingKeyFrames"/> object, making deep copies of this object's current values. Resource references, data bindings, and animations are not copied, but their current values are.
    /// </summary>
    /// 
    /// <returns>
    /// A modifiable clone of the current object. The cloned object's <see cref="P:System.Windows.Freezable.IsFrozen"/> property will be false even if the source's <see cref="P:System.Windows.Freezable.IsFrozen"/> property was true.
    /// </returns>
    public WriteableBitmap CloneCurrentValue()
    {
      return (WriteableBitmap) base.CloneCurrentValue();
    }

    /// <summary>
    /// Reserves the back buffer for updates.
    /// </summary>
    public void Lock()
    {
      this.TryLock(Duration.Forever);
    }

    /// <summary>
    /// Attempts to lock the bitmap, waiting for no longer than the specified length of time.
    /// </summary>
    /// 
    /// <returns>
    /// true if the lock was acquired; otherwise, false.
    /// </returns>
    /// <param name="timeout">A <see cref="T:System.Windows.Duration"/> that represents the length of time to wait. A value of 0 returns immediately. A value of <see cref="P:System.Windows.Duration.Forever"/> blocks indefinitely. </param><exception cref="T:System.ArgumentOutOfRangeException"><paramref name="timeout"/> is set to <see cref="P:System.Windows.Duration.Automatic"/>. </exception>
    [SecurityCritical]
    public bool TryLock(Duration timeout)
    {
      this.WritePreamble();
      if (timeout == Duration.Automatic)
        throw new ArgumentOutOfRangeException("timeout");
      TimeSpan timeout1 = !(timeout == Duration.Forever) ? timeout.TimeSpan : TimeSpan.FromMilliseconds(-1.0);
      if ((int) this._lockCount == -1)
        throw new InvalidOperationException(MS.Internal.PresentationCore.SR.Get("Image_LockCountLimit"));
      if ((int) this._lockCount == 0)
      {
        if (!this.AcquireBackBuffer(timeout1, true))
          return false;
        Int32Rect prcLock = new Int32Rect(0, 0, this._pixelWidth, this._pixelHeight);
        MS.Internal.HRESULT.Check(MS.Win32.PresentationCore.UnsafeNativeMethods.WICBitmap.Lock((SafeMILHandle) this.WicSourceHandle, ref prcLock, MS.Internal.LockFlags.MIL_LOCK_WRITE, out this._pBackBufferLock));
        if (this._backBuffer == IntPtr.Zero)
        {
          IntPtr ppbData = IntPtr.Zero;
          uint pcbBufferSize = 0U;
          MS.Internal.HRESULT.Check(MS.Win32.PresentationCore.UnsafeNativeMethods.WICBitmapLock.GetDataPointer(this._pBackBufferLock, ref pcbBufferSize, ref ppbData));
          this.BackBuffer = ppbData;
          uint pcbStride = 0U;
          MS.Internal.HRESULT.Check(MS.Win32.PresentationCore.UnsafeNativeMethods.WICBitmapLock.GetStride(this._pBackBufferLock, ref pcbStride));
          Invariant.Assert(pcbStride <= (uint) int.MaxValue);
          this._backBufferStride.Value = (int) pcbStride;
        }
        this.UnsubscribeFromCommittingBatch();
      }
      ++this._lockCount;
      return true;
    }

    /// <summary>
    /// Releases the back buffer to make it available for display.
    /// </summary>
    /// <exception cref="T:System.InvalidOperationException">The bitmap has not been locked by a call to the <see cref="M:System.Windows.Media.Imaging.WriteableBitmap.Lock"/> or <see cref="M:System.Windows.Media.Imaging.WriteableBitmap.TryLock(System.Windows.Duration)"/> methods. </exception>
    [SecurityCritical]
    public void Unlock()
    {
      this.WritePreamble();
      if ((int) this._lockCount == 0)
        throw new InvalidOperationException(MS.Internal.PresentationCore.SR.Get("Image_MustBeLocked"));
      Invariant.Assert(this._lockCount > 0U, "Lock count should never be negative!");
      --this._lockCount;
      if ((int) this._lockCount != 0)
        return;
      this._pBackBufferLock.Dispose();
      this._pBackBufferLock = (SafeMILHandle) null;
      if (!this._hasDirtyRects)
        return;
      this.SubscribeToCommittingBatch();
      this.WritePostscript();
    }

    /// <summary>
    /// Updates the pixels in the specified region of the bitmap.
    /// </summary>
    /// <param name="sourceRect">The rectangle in <paramref name="sourceBuffer"/> to copy.</param><param name="sourceBuffer">The input buffer used to update the bitmap.</param><param name="sourceBufferSize">The size of the input buffer.</param><param name="sourceBufferStride">The stride of the input buffer, in bytes.</param><param name="destinationX">The destination x-coordinate of the left-most pixel in the back buffer.</param><param name="destinationY">The destination y-coordinate of the top-most pixel in the back buffer.</param><exception cref="T:System.ArgumentOutOfRangeException">One or more of the following conditions is true.<paramref name="sourceRect"/> falls outside the bounds of the <see cref="T:System.Windows.Media.Imaging.WriteableBitmap"/>. <paramref name="destinationX"/> or <paramref name="destinationY"/> is outside the bounds of the <see cref="T:System.Windows.Media.Imaging.WriteableBitmap"/>.<paramref name="sourceBufferSize"/> &lt; 1 <paramref name="sourceBufferStride"/> &lt; 1 </exception><exception cref="T:System.ArgumentNullException"><paramref name="sourceBuffer"/> is null.</exception>
    [SecurityCritical]
    public void WritePixels(Int32Rect sourceRect, IntPtr sourceBuffer, int sourceBufferSize, int sourceBufferStride, int destinationX, int destinationY)
    {
      SecurityHelper.DemandUnmanagedCode();
      this.WritePreamble();
      this.WritePixelsImpl(sourceRect, sourceBuffer, sourceBufferSize, sourceBufferStride, destinationX, destinationY, false);
    }

    /// <summary>
    /// Updates the pixels in the specified region of the bitmap.
    /// </summary>
    /// <param name="sourceRect">The rectangle in <paramref name="sourceBuffer"/> to copy.</param><param name="sourceBuffer">The input buffer used to update the bitmap.</param><param name="sourceBufferStride">The stride of the input buffer, in bytes.</param><param name="destinationX">The destination x-coordinate of the left-most pixel in the back buffer.</param><param name="destinationY">The destination y-coordinate of the top-most pixel in the back buffer.</param><exception cref="T:System.ArgumentOutOfRangeException">One or more of the following conditions is true.<paramref name="sourceRect"/> falls outside the bounds of the <see cref="T:System.Windows.Media.Imaging.WriteableBitmap"/>.<paramref name="destinationX"/> or <paramref name="destinationY"/> is outside the bounds of the <see cref="T:System.Windows.Media.Imaging.WriteableBitmap"/>.  <paramref name="sourceBufferStride"/> &lt; 1 </exception><exception cref="T:System.ArgumentNullException"><paramref name="sourceBuffer"/> is null.</exception><exception cref="T:System.ArgumentException"><paramref name="sourceBuffer"/> has a rank other than 1 or 2, or its length is less than or equal to 0.</exception>
    [SecurityCritical]
    public void WritePixels(Int32Rect sourceRect, Array sourceBuffer, int sourceBufferStride, int destinationX, int destinationY)
    {
      this.WritePreamble();
      int elementSize;
      int sourceBufferSize;
      System.Type elementType;
      this.ValidateArrayAndGetInfo(sourceBuffer, false, out elementSize, out sourceBufferSize, out elementType);
      if (elementType == (System.Type) null || !elementType.IsValueType)
        throw new ArgumentException(MS.Internal.PresentationCore.SR.Get("Image_InvalidArrayForPixel"));
      GCHandle gcHandle = GCHandle.Alloc((object) sourceBuffer, GCHandleType.Pinned);
      try
      {
        IntPtr sourceBuffer1 = gcHandle.AddrOfPinnedObject();
        this.WritePixelsImpl(sourceRect, sourceBuffer1, sourceBufferSize, sourceBufferStride, destinationX, destinationY, false);
      }
      finally
      {
        gcHandle.Free();
      }
    }

    /// <summary>
    /// Updates the pixels in the specified region of the bitmap.
    /// </summary>
    /// <param name="sourceRect">The rectangle of the <see cref="T:System.Windows.Media.Imaging.WriteableBitmap"/> to update.</param><param name="buffer">The input buffer used to update the bitmap.</param><param name="bufferSize">The size of the input buffer.</param><param name="stride">The stride of the update region in <paramref name="buffer"/>.</param><exception cref="T:System.ArgumentOutOfRangeException">One or more of the following conditions is true.<paramref name="sourceRect"/> falls outside the bounds of the <see cref="T:System.Windows.Media.Imaging.WriteableBitmap"/>.<paramref name="bufferSize"/> &lt; 1 <paramref name="stride"/> &lt; 1 </exception><exception cref="T:System.ArgumentNullException"><paramref name="buffer"/> is null.</exception>
    [SecurityCritical]
    public void WritePixels(Int32Rect sourceRect, IntPtr buffer, int bufferSize, int stride)
    {
      SecurityHelper.DemandUnmanagedCode();
      this.WritePreamble();
      if (bufferSize < 1)
        throw new ArgumentOutOfRangeException("bufferSize", MS.Internal.PresentationCore.SR.Get("ParameterCannotBeLessThan", new object[1]
        {
          (object) 1
        }));
      else if (stride < 1)
      {
        throw new ArgumentOutOfRangeException("stride", MS.Internal.PresentationCore.SR.Get("ParameterCannotBeLessThan", new object[1]
        {
          (object) 1
        }));
      }
      else
      {
        if (sourceRect.IsEmpty || sourceRect.Width <= 0 || sourceRect.Height <= 0)
          return;
        int x = sourceRect.X;
        int y = sourceRect.Y;
        sourceRect.X = 0;
        sourceRect.Y = 0;
        this.WritePixelsImpl(sourceRect, buffer, bufferSize, stride, x, y, true);
      }
    }

    /// <summary>
    /// Updates the pixels in the specified region of the bitmap.
    /// </summary>
    /// <param name="sourceRect">The rectangle of the <see cref="T:System.Windows.Media.Imaging.WriteableBitmap"/> to update.</param><param name="pixels">The pixel array used to update the bitmap.</param><param name="stride">The stride of the update region in <paramref name="pixels"/>.</param><param name="offset">The input buffer offset.</param><exception cref="T:System.ArgumentOutOfRangeException">One or more of the following conditions is true.<paramref name="sourceRect"/> falls outside the bounds of the <see cref="T:System.Windows.Media.Imaging.WriteableBitmap"/>.  <paramref name="stride"/> &lt; 1 <paramref name="offset"/> &lt; 0 </exception><exception cref="T:System.ArgumentNullException"><paramref name="pixels"/> is null.</exception><exception cref="T:System.ArgumentException"><paramref name="pixels"/> has a rank other than 1 or 2, or its length is less than or equal to 0.</exception>
    [SecurityCritical]
    public void WritePixels(Int32Rect sourceRect, Array pixels, int stride, int offset)
    {
      this.WritePreamble();
      if (sourceRect.IsEmpty || sourceRect.Width <= 0 || sourceRect.Height <= 0)
        return;
      int elementSize;
      int sourceBufferSize;
      System.Type elementType;
      this.ValidateArrayAndGetInfo(pixels, true, out elementSize, out sourceBufferSize, out elementType);
      if (stride < 1)
        throw new ArgumentOutOfRangeException("stride", MS.Internal.PresentationCore.SR.Get("ParameterCannotBeLessThan", new object[1]
        {
          (object) 1
        }));
      else if (offset < 0)
      {
        throw new ArgumentOutOfRangeException("offset", MS.Internal.PresentationCore.SR.Get("ParameterCannotBeLessThan", new object[1]
        {
          (object) 0
        }));
      }
      else
      {
        if (elementType == (System.Type) null || !elementType.IsValueType)
          throw new ArgumentException(MS.Internal.PresentationCore.SR.Get("Image_InvalidArrayForPixel"));
        int num = checked (offset * elementSize);
        if (num >= sourceBufferSize)
          throw new IndexOutOfRangeException();
        int x = sourceRect.X;
        int y = sourceRect.Y;
        sourceRect.X = 0;
        sourceRect.Y = 0;
        GCHandle gcHandle = GCHandle.Alloc((object) pixels, GCHandleType.Pinned);
        try
        {
          IntPtr sourceBuffer = gcHandle.AddrOfPinnedObject();
          sourceBuffer = new IntPtr(checked ((long) sourceBuffer + (long) num));
          checked { sourceBufferSize -= num; }
          this.WritePixelsImpl(sourceRect, sourceBuffer, sourceBufferSize, stride, x, y, true);
        }
        finally
        {
          gcHandle.Free();
        }
      }
    }

    protected override Freezable CreateInstanceCore()
    {
      return (Freezable) new WriteableBitmap();
    }

    [SecurityCritical]
    [SecurityTreatAsSafe]
    protected override void CloneCore(Freezable sourceFreezable)
    {
      WriteableBitmap sourceBitmap = (WriteableBitmap) sourceFreezable;
      base.CloneCore(sourceFreezable);
      this.CopyCommon(sourceBitmap);
    }

    [SecurityTreatAsSafe]
    [SecurityCritical]
    protected override bool FreezeCore(bool isChecking)
    {
      bool flag = (int) this._lockCount == 0 && base.FreezeCore(isChecking);
      if (flag && !isChecking)
      {
        MS.Internal.HRESULT.Check(MILSwDoubleBufferedBitmap.ProtectBackBuffer(this._pDoubleBufferedBitmap));
        this.AcquireBackBuffer(TimeSpan.Zero, false);
        this._needsUpdate = true;
        this._hasDirtyRects = false;
        this.WicSourceHandle.CopyMemoryPressure(this._pDoubleBufferedBitmap);
        this._actLikeSimpleBitmap = true;
        int channelCount = this._duceResource.GetChannelCount();
        for (int index1 = 0; index1 < channelCount; ++index1)
        {
          DUCE.IResource resource = (DUCE.IResource) this;
          DUCE.Channel channel = this._duceResource.GetChannel(index1);
          uint refCountOnChannel = this._duceResource.GetRefCountOnChannel(channel);
          for (uint index2 = 0U; index2 < refCountOnChannel; ++index2)
            resource.ReleaseOnChannel(channel);
          for (uint index2 = 0U; index2 < refCountOnChannel; ++index2)
            resource.AddRefOnChannel(channel);
        }
        this._pDoubleBufferedBitmap.Dispose();
        this._pDoubleBufferedBitmap = (SafeMILHandle) null;
        this._copyCompletedEvent.Close();
        this._copyCompletedEvent = (ManualResetEvent) null;
        this._committingBatchHandler = (EventHandler) null;
        this._pBackBuffer = (BitmapSourceSafeMILHandle) null;
      }
      return flag;
    }

    [SecurityTreatAsSafe]
    [SecurityCritical]
    protected override void CloneCurrentValueCore(Freezable sourceFreezable)
    {
      WriteableBitmap sourceBitmap = (WriteableBitmap) sourceFreezable;
      base.CloneCurrentValueCore(sourceFreezable);
      this.CopyCommon(sourceBitmap);
    }

    [SecurityCritical]
    [SecurityTreatAsSafe]
    protected override void GetAsFrozenCore(Freezable sourceFreezable)
    {
      WriteableBitmap sourceBitmap = (WriteableBitmap) sourceFreezable;
      base.GetAsFrozenCore(sourceFreezable);
      this.CopyCommon(sourceBitmap);
    }

    [SecurityTreatAsSafe]
    [SecurityCritical]
    protected override void GetCurrentValueAsFrozenCore(Freezable sourceFreezable)
    {
      WriteableBitmap sourceBitmap = (WriteableBitmap) sourceFreezable;
      base.GetCurrentValueAsFrozenCore(sourceFreezable);
      this.CopyCommon(sourceBitmap);
    }

    [SecurityCritical]
    internal override void FinalizeCreation()
    {
      this.IsSourceCached = true;
      this.CreationCompleted = true;
      this.UpdateCachedSettings();
    }

    internal override DUCE.ResourceHandle AddRefOnChannelCore(DUCE.Channel channel)
    {
      if (this._actLikeSimpleBitmap)
        return base.AddRefOnChannelCore(channel);
      if (this._duceResource.CreateOrAddRefOnChannel((object) this, channel, DUCE.ResourceType.TYPE_DOUBLEBUFFEREDBITMAP))
      {
        if (!channel.IsSynchronous && this._hasDirtyRects)
          this.SubscribeToCommittingBatch();
        this.AddRefOnChannelAnimations(channel);
        this.UpdateResource(channel, true);
      }
      return this._duceResource.GetHandle(channel);
    }

    internal override void ReleaseOnChannelCore(DUCE.Channel channel)
    {
      if (!this._duceResource.ReleaseOnChannel(channel))
        return;
      if (!channel.IsSynchronous)
        this.UnsubscribeFromCommittingBatch();
      this.ReleaseOnChannelAnimations(channel);
    }

    [SecurityCritical]
    [SecurityTreatAsSafe]
    internal override unsafe void UpdateBitmapSourceResource(DUCE.Channel channel, bool skipOnChannelCheck)
    {
      if (this._actLikeSimpleBitmap)
      {
        base.UpdateBitmapSourceResource(channel, skipOnChannelCheck);
      }
      else
      {
        if (!skipOnChannelCheck && !this._duceResource.IsOnChannel(channel))
          return;
        DUCE.MILCMD_DOUBLEBUFFEREDBITMAP doublebufferedbitmap;
        doublebufferedbitmap.Type = MILCMD.MilCmdDoubleBufferedBitmap;
        doublebufferedbitmap.Handle = this._duceResource.GetHandle(channel);
        doublebufferedbitmap.SwDoubleBufferedBitmap = (ulong) this._pDoubleBufferedBitmap.DangerousGetHandle().ToPointer();
        doublebufferedbitmap.UseBackBuffer = channel.IsSynchronous ? 1U : 0U;
        int num = (int) MS.Win32.PresentationCore.UnsafeNativeMethods.MILUnknown.AddRef(this._pDoubleBufferedBitmap);
        channel.SendCommand((byte*) &doublebufferedbitmap, sizeof (DUCE.MILCMD_DOUBLEBUFFEREDBITMAP), false);
      }
    }

    private long GetEstimatedSize(int pixelWidth, int pixelHeight, PixelFormat pixelFormat)
    {
      return (long) (pixelWidth * pixelHeight * pixelFormat.InternalBitsPerPixel / 8 * 2);
    }

    [SecurityCritical]
    private void InitFromBitmapSource(BitmapSource source)
    {
      if (source == null)
        throw new ArgumentNullException("source");
      if (source.PixelWidth < 0)
        MS.Internal.HRESULT.Check(-2147024362);
      if (source.PixelHeight < 0)
        MS.Internal.HRESULT.Check(-2147024362);
      this.BeginInit();
      this._syncObject = source.SyncObject;
      lock (this._syncObject)
      {
        Guid local_6 = source.Format.Guid;
        SafeMILHandle local_1 = new SafeMILHandle();
        if (source.Format.Palettized)
          local_1 = source.Palette.InternalPalette;
        MS.Internal.HRESULT.Check(MILSwDoubleBufferedBitmap.Create((uint) source.PixelWidth, (uint) source.PixelHeight, source.DpiX, source.DpiY, ref local_6, local_1, out this._pDoubleBufferedBitmap));
        this._pDoubleBufferedBitmap.UpdateEstimatedSize(this.GetEstimatedSize(source.PixelWidth, source.PixelHeight, source.Format));
        this.Lock();
        Int32Rect local_0 = new Int32Rect(0, 0, this._pixelWidth, this._pixelHeight);
        int local_2 = checked (this._backBufferStride.Value * source.PixelHeight);
        source.CriticalCopyPixels(local_0, this._backBuffer, local_2, this._backBufferStride.Value);
        this.AddDirtyRect(local_0);
        this.Unlock();
      }
      this.EndInit();
    }

    [SecurityCritical]
    private unsafe void WritePixelsImpl(Int32Rect sourceRect, IntPtr sourceBuffer, int sourceBufferSize, int sourceBufferStride, int destinationX, int destinationY, bool backwardsCompat)
    {
      if (sourceRect.X < 0)
        throw new ArgumentOutOfRangeException("sourceRect", MS.Internal.PresentationCore.SR.Get("ParameterCannotBeNegative"));
      if (sourceRect.Y < 0)
        throw new ArgumentOutOfRangeException("sourceRect", MS.Internal.PresentationCore.SR.Get("ParameterCannotBeNegative"));
      if (sourceRect.Width < 0)
      {
        throw new ArgumentOutOfRangeException("sourceRect", MS.Internal.PresentationCore.SR.Get("ParameterMustBeBetween", (object) 0, (object) this._pixelWidth));
      }
      else
      {
        if (sourceRect.Width > this._pixelWidth)
        {
          if (backwardsCompat)
            MS.Internal.HRESULT.Check(-2147024809);
          else
            throw new ArgumentOutOfRangeException("sourceRect", MS.Internal.PresentationCore.SR.Get("ParameterMustBeBetween", (object) 0, (object) this._pixelWidth));
        }
        if (sourceRect.Height < 0)
        {
          throw new ArgumentOutOfRangeException("sourceRect", MS.Internal.PresentationCore.SR.Get("ParameterMustBeBetween", (object) 0, (object) this._pixelHeight));
        }
        else
        {
          if (sourceRect.Height > this._pixelHeight)
          {
            if (backwardsCompat)
              MS.Internal.HRESULT.Check(-2147024809);
            else
              throw new ArgumentOutOfRangeException("sourceRect", MS.Internal.PresentationCore.SR.Get("ParameterMustBeBetween", (object) 0, (object) this._pixelHeight));
          }
          if (destinationX < 0)
          {
            if (!backwardsCompat)
              throw new ArgumentOutOfRangeException("sourceRect", MS.Internal.PresentationCore.SR.Get("ParameterCannotBeNegative"));
            MS.Internal.HRESULT.Check(-2147024362);
          }
          if (destinationX > this._pixelWidth - sourceRect.Width)
          {
            if (backwardsCompat)
              MS.Internal.HRESULT.Check(-2147024809);
            else
              throw new ArgumentOutOfRangeException("destinationX", MS.Internal.PresentationCore.SR.Get("ParameterMustBeBetween", (object) 0, (object) (this._pixelWidth - sourceRect.Width)));
          }
          if (destinationY < 0)
          {
            if (backwardsCompat)
              MS.Internal.HRESULT.Check(-2147024362);
            else
              throw new ArgumentOutOfRangeException("destinationY", MS.Internal.PresentationCore.SR.Get("ParameterMustBeBetween", (object) 0, (object) (this._pixelHeight - sourceRect.Height)));
          }
          if (destinationY > this._pixelHeight - sourceRect.Height)
          {
            if (backwardsCompat)
              MS.Internal.HRESULT.Check(-2147024809);
            else
              throw new ArgumentOutOfRangeException("destinationY", MS.Internal.PresentationCore.SR.Get("ParameterMustBeBetween", (object) 0, (object) (this._pixelHeight - sourceRect.Height)));
          }
          if (sourceBuffer == IntPtr.Zero)
            throw new ArgumentNullException(backwardsCompat ? "buffer" : "sourceBuffer");
          if (sourceBufferStride < 1)
          {
            throw new ArgumentOutOfRangeException("sourceBufferStride", MS.Internal.PresentationCore.SR.Get("ParameterCannotBeLessThan", new object[1]
            {
              (object) 1
            }));
          }
          else
          {
            if (sourceRect.Width == 0 || sourceRect.Height == 0)
              return;
            uint num1 = checked ((uint) ((sourceRect.X + sourceRect.Width) * this._format.InternalBitsPerPixel) + 7U) / 8U;
            uint num2 = checked ((uint) ((sourceRect.Y + sourceRect.Height - 1) * sourceBufferStride) + num1);
            if ((long) sourceBufferSize < (long) num2)
            {
              if (!backwardsCompat)
                throw new ArgumentException(MS.Internal.PresentationCore.SR.Get("Image_InsufficientBufferSize"), "sourceBufferSize");
              MS.Internal.HRESULT.Check(-2003292276);
            }
            uint copyWidthInBits = checked ((uint) (sourceRect.Width * this._format.InternalBitsPerPixel));
            uint num3 = checked ((uint) unchecked (checked (sourceRect.X * this._format.InternalBitsPerPixel) / 8));
            uint inputBufferOffsetInBits = checked ((uint) unchecked (checked (sourceRect.X * this._format.InternalBitsPerPixel) % 8));
            uint num4 = checked ((uint) ((long) (sourceRect.Y * sourceBufferStride) + (long) num3));
            uint num5 = checked ((uint) unchecked (checked (destinationX * this._format.InternalBitsPerPixel) / 8));
            uint outputBufferOffsetInBits = checked ((uint) unchecked (checked (destinationX * this._format.InternalBitsPerPixel) % 8));
            Int32Rect dirtyRect = sourceRect;
            dirtyRect.X = destinationX;
            dirtyRect.Y = destinationY;
            uint num6 = checked ((uint) (destinationY * this._backBufferStride.Value) + num5);
            byte* pOutputBuffer = (byte*) checked ((IntPtr) this._backBuffer.ToPointer() + unchecked ((int) num6));
            uint outputBufferSize = checked (this._backBufferSize - num6);
            byte* pInputBuffer = (byte*) checked ((IntPtr) sourceBuffer.ToPointer() + unchecked ((int) num4));
            uint inputBufferSize = checked ((uint) sourceBufferSize - num4);
            this.Lock();
            MILUtilities.MILCopyPixelBuffer(pOutputBuffer, outputBufferSize, checked ((uint) this._backBufferStride.Value), outputBufferOffsetInBits, pInputBuffer, inputBufferSize, checked ((uint) sourceBufferStride), inputBufferOffsetInBits, checked ((uint) sourceRect.Height), copyWidthInBits);
            this.AddDirtyRect(dirtyRect);
            this.Unlock();
          }
        }
      }
    }

    [SecurityCritical]
    private bool AcquireBackBuffer(TimeSpan timeout, bool waitForCopy)
    {
      bool flag1 = false;
      if (this._pBackBuffer == null)
      {
        bool flag2 = true;
        if (waitForCopy)
          flag2 = this._copyCompletedEvent.WaitOne(timeout, false);
        if (flag2)
        {
          MILSwDoubleBufferedBitmap.GetBackBuffer(this._pDoubleBufferedBitmap, out this._pBackBuffer, out this._backBufferSize);
          this._syncObject = (object) (this.WicSourceHandle = this._pBackBuffer);
          flag1 = true;
        }
      }
      else
        flag1 = true;
      return flag1;
    }

    [SecurityCritical]
    private void CopyCommon(WriteableBitmap sourceBitmap)
    {
      this.Animatable_IsResourceInvalidationNecessary = false;
      this._actLikeSimpleBitmap = false;
      this.InitFromBitmapSource((BitmapSource) sourceBitmap);
      this.Animatable_IsResourceInvalidationNecessary = true;
    }

    private void BeginInit()
    {
      this._bitmapInit.BeginInit();
    }

    [SecurityTreatAsSafe]
    [SecurityCritical]
    private void EndInit()
    {
      this._bitmapInit.EndInit();
      this.FinalizeCreation();
    }

    [SecurityCritical]
    private void ValidateArrayAndGetInfo(Array sourceBuffer, bool backwardsCompat, out int elementSize, out int sourceBufferSize, out System.Type elementType)
    {
      if (sourceBuffer == null)
        throw new ArgumentNullException(backwardsCompat ? "pixels" : "sourceBuffer");
      if (sourceBuffer.Rank == 1)
      {
        if (sourceBuffer.GetLength(0) <= 0)
        {
          if (!backwardsCompat)
            throw new ArgumentException(MS.Internal.PresentationCore.SR.Get("Image_InsufficientBuffer"), "sourceBuffer");
          elementSize = 1;
          sourceBufferSize = 0;
          elementType = (System.Type) null;
        }
        else
        {
          object structure = sourceBuffer.GetValue(0);
          elementSize = Marshal.SizeOf(structure);
          sourceBufferSize = checked (sourceBuffer.GetLength(0) * elementSize);
          elementType = structure.GetType();
        }
      }
      else
      {
        if (sourceBuffer.Rank != 2)
          throw new ArgumentException(MS.Internal.PresentationCore.SR.Get("Collection_BadRank"), backwardsCompat ? "pixels" : "sourceBuffer");
        if (sourceBuffer.GetLength(0) <= 0 || sourceBuffer.GetLength(1) <= 0)
        {
          if (!backwardsCompat)
            throw new ArgumentException(MS.Internal.PresentationCore.SR.Get("Image_InsufficientBuffer"), "sourceBuffer");
          elementSize = 1;
          sourceBufferSize = 0;
          elementType = (System.Type) null;
        }
        else
        {
          object structure = sourceBuffer.GetValue(0, 0);
          elementSize = Marshal.SizeOf(structure);
          sourceBufferSize = checked (sourceBuffer.GetLength(0) * sourceBuffer.GetLength(1) * elementSize);
          elementType = structure.GetType();
        }
      }
    }

    private void SubscribeToCommittingBatch()
    {
      if (this._isWaitingForCommit)
        return;
      MediaContext mediaContext = MediaContext.From(this.Dispatcher);
      if (!this._duceResource.IsOnChannel(mediaContext.Channel))
        return;
      mediaContext.CommittingBatch += this.CommittingBatchHandler;
      this._isWaitingForCommit = true;
    }

    private void UnsubscribeFromCommittingBatch()
    {
      if (!this._isWaitingForCommit)
        return;
      MediaContext.From(this.Dispatcher).CommittingBatch -= this.CommittingBatchHandler;
      this._isWaitingForCommit = false;
    }

    [SecurityCritical]
    [SecurityTreatAsSafe]
    private unsafe void OnCommittingBatch(object sender, EventArgs args)
    {
      this.UnsubscribeFromCommittingBatch();
      this._copyCompletedEvent.Reset();
      this._pBackBuffer = (BitmapSourceSafeMILHandle) null;
      DUCE.Channel channel = sender as DUCE.Channel;
      IntPtr currentProcess = MS.Win32.UnsafeNativeMethods.GetCurrentProcess();
      IntPtr hTargetHandle;
      if (!MS.Win32.UnsafeNativeMethods.DuplicateHandle(currentProcess, this._copyCompletedEvent.SafeWaitHandle, currentProcess, out hTargetHandle, 0U, false, 2U))
        throw new Win32Exception();
      DUCE.MILCMD_DOUBLEBUFFEREDBITMAP_COPYFORWARD doublebufferedbitmapCopyforward;
      doublebufferedbitmapCopyforward.Type = MILCMD.MilCmdDoubleBufferedBitmapCopyForward;
      doublebufferedbitmapCopyforward.Handle = this._duceResource.GetHandle(channel);
      doublebufferedbitmapCopyforward.CopyCompletedEvent = (ulong) hTargetHandle.ToInt64();
      channel.SendCommand((byte*) &doublebufferedbitmapCopyforward, sizeof (DUCE.MILCMD_DOUBLEBUFFEREDBITMAP_COPYFORWARD));
      channel.CloseBatch();
      this._hasDirtyRects = false;
    }
  }
}
