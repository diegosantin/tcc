﻿// Type: Microsoft.Kinect.DepthImageFrame
// Assembly: Microsoft.Kinect, Version=1.7.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
// Assembly location: C:\Program Files\Microsoft SDKs\Kinect\v1.7\Assemblies\Microsoft.Kinect.dll

using System;
using System.Runtime.InteropServices;

namespace Microsoft.Kinect
{
  /// <summary>
  /// A frame used specifically for depth images.
  ///             It provides access to the dimensions, format and pixel data for a depth frame,
  ///             and allows for mapping of coordinates between skeleton frames and color frames.
  /// 
  /// </summary>
  public sealed class DepthImageFrame : ImageFrame
  {
    /// <summary>
    /// The lock object used to synchronize access as necessary.
    /// 
    /// </summary>
    private readonly object _dataAccessLock = new object();
    /// <summary>
    /// A bitmask for extracting the player index bit field from the depth value.
    /// 
    /// </summary>
    public const int PlayerIndexBitmask = 7;
    /// <summary>
    /// The width of the player index bitmask.
    /// 
    /// </summary>
    public const int PlayerIndexBitmaskWidth = 3;
    /// <summary>
    /// The underlying depth image stream.
    /// 
    /// </summary>
    private readonly DepthImageStream _depthImageStream;
    /// <summary>
    /// The underlying depth frame data.
    /// 
    /// </summary>
    private DataPool<int, ImageType, ImageResolution, short[], DepthImagePixel[]>.Entry _frameData;

    /// <summary>
    /// Gets this frame's Framerate and Resolution.
    /// 
    /// </summary>
    public DepthImageFormat Format { get; private set; }

    /// <summary>
    /// Gets the depth sensor range with which this frame was captured.
    /// 
    /// </summary>
    public DepthRange Range
    {
      get
      {
        return !this.FrameFlags.HasFlag((Enum) ImageFrameFlags.NUI_IMAGE_FRAME_FLAG_NEAR_MODE_ENABLED) ? DepthRange.Default : DepthRange.Near;
      }
    }

    /// <summary>
    /// Gets the maximum reliable Depth value in mm for the depth sensor range setting used to capture this frame.
    /// 
    /// </summary>
    public int MaxDepth
    {
      get
      {
        return this.Range != DepthRange.Near ? 4000 : 3000;
      }
    }

    /// <summary>
    /// Gets the minimum reliable Depth value in mm for the depth sensor range setting used to capture this frame.
    /// 
    /// </summary>
    public int MinDepth
    {
      get
      {
        return this.Range != DepthRange.Near ? 800 : 400;
      }
    }

    /// <summary>
    /// Total length of the pixel data buffer of this ImageFrame.
    /// 
    /// </summary>
    public override int PixelDataLength
    {
      get
      {
        return DepthImageStream.LookUpPixelDataLength(this.Format);
      }
    }

    /// <summary>
    /// The frame's source stream.
    /// 
    /// </summary>
    internal override ImageStream SourceStream
    {
      get
      {
        return (ImageStream) this._depthImageStream;
      }
    }

    /// <summary>
    /// Initializes a new instance of the DepthImageFrame class.
    /// 
    /// </summary>
    /// <param name="depthImageStream">The related depth stream.</param><param name="frameNumber">The current frame number.</param><param name="timestamp">The current timestamp.</param><param name="frameFlags">The current frame flags.</param><param name="pixelData">The pixel data.</param>
    private DepthImageFrame(DepthImageStream depthImageStream, int frameNumber, long timestamp, ImageFrameFlags frameFlags, DataPool<int, ImageType, ImageResolution, short[], DepthImagePixel[]>.Entry pixelData)
      : base(pixelData.Value1, pixelData.Value2, frameNumber, timestamp, frameFlags)
    {
      this._depthImageStream = depthImageStream;
      this._frameData = pixelData;
      this.Format = DepthImageStream.LookUpDepthImageFormat(pixelData.Value1, pixelData.Value2);
    }

    /// <summary>
    /// Construct a DepthImageFrame for the frameNumber provided, if possible.
    ///             If the data is not available for the specified frameNumber, this returns null.
    ///             If this method succeeds, the result must be disposed.
    /// 
    /// </summary>
    /// <param name="depthImageStream">The associated DepthImageStream.</param><param name="frameNumber">The frameNumber to retrieve.</param><param name="timestamp">The timestamp of the frame to retrieve.</param><param name="frameFlags">The flags for the frame.</param>
    /// <returns>
    /// Null upon failure, a DepthImageStream upon success, which must be Disposed.
    /// </returns>
    internal static DepthImageFrame Create(DepthImageStream depthImageStream, int frameNumber, long timestamp, ImageFrameFlags frameFlags)
    {
      DepthImageFrame depthImageFrame = (DepthImageFrame) null;
      DataPool<int, ImageType, ImageResolution, short[], DepthImagePixel[]>.Entry pixelData = depthImageStream.LockPixelData(frameNumber);
      if (pixelData != null)
        depthImageFrame = new DepthImageFrame(depthImageStream, frameNumber, timestamp, frameFlags, pixelData);
      return depthImageFrame;
    }

    /// <summary>
    /// Private helper function used by CopyPixelDataTo and Copy DepthImagePixelDataTo.
    /// 
    /// </summary>
    /// <typeparam name="T">Type of elements to copy.</typeparam><param name="pixelData">Destination array.</param><param name="copyFunction">Function to perform the copy.</param>
    private void CopyPixelDataTo<T>(T[] pixelData, Action<T[], int> copyFunction)
    {
      if (pixelData == null)
        throw new ArgumentNullException("pixelData");
      if (pixelData.Length != this.PixelDataLength)
        throw new ArgumentException(Resources.PixelBufferIncorrectLength, "pixelData");
      this.CopyPixelDataTo<T[]>(pixelData, pixelData.Length, copyFunction);
    }

    /// <summary>
    /// Private helper function used by CopyPixelDataTo and Copy DepthImagePixelDataTo.
    /// 
    /// </summary>
    /// <typeparam name="T">Type of data to copy.</typeparam><param name="pixelData">Destination array.</param><param name="pixelDataLength">The length of the destination array.</param><param name="copyFunction">Function to perform the copy.</param>
    private void CopyPixelDataTo<T>(T pixelData, int pixelDataLength, Action<T, int> copyFunction)
    {
      if (pixelDataLength != this.PixelDataLength)
        throw new ArgumentException(Resources.PixelBufferIncorrectLength, "pixelDataLength");
      lock (this._dataAccessLock)
      {
        if (this._frameData == null)
          throw new ObjectDisposedException("DepthImageFrame");
        copyFunction(pixelData, pixelDataLength);
      }
    }

    /// <summary>
    /// This method copies the frame's pixel data to a pre-allocated pixel array.
    /// 
    /// </summary>
    /// <param name="pixelData">The pixel array to receive the data.
    ///             It must be exactly PixelDataLength in length.</param>
    public unsafe void CopyPixelDataTo(short[] pixelData)
    {
      this.CopyPixelDataTo<short>(pixelData, (Action<short[], int>) ((data, length) =>
      {
        fixed (short* fixed_0 = this._frameData.Value3)
          Marshal.Copy((IntPtr) ((void*) fixed_0), data, 0, length);
      }));
    }

    /// <summary>
    /// This method copies the frame's pixel data to a pre-allocated pixel array.
    /// 
    /// </summary>
    /// <param name="pixelData">The pixel array to receive the data.
    ///             It must be exactly PixelDataLength in length.</param>
    public unsafe void CopyDepthImagePixelDataTo(DepthImagePixel[] pixelData)
    {
      this.CopyPixelDataTo<DepthImagePixel>(pixelData, (Action<DepthImagePixel[], int>) ((data, length) =>
      {
        fixed (DepthImagePixel* fixed_0 = this._frameData.Value4)
          fixed (DepthImagePixel* fixed_1 = data)
            Microsoft.Kinect.NativeMethods.CopyMemory((IntPtr) ((void*) fixed_1), (IntPtr) ((void*) fixed_0), length * sizeof (DepthImagePixel));
      }));
    }

    /// <summary>
    /// This method copies the frame's pixel data to a pre-allocated byte array.
    /// 
    /// </summary>
    /// <param name="pixelData">The IntPtr of the byte array.</param><param name="pixelDataLength">The count of Int16s to copy to pixelData.  This must be equal to the frame’s PixelDataLength.</param>
    public void CopyPixelDataTo(IntPtr pixelData, int pixelDataLength)
    {
      if (pixelData == IntPtr.Zero)
        throw new ArgumentNullException("pixelData");
      this.CopyPixelDataTo<IntPtr>(pixelData, pixelDataLength, (Action<IntPtr, int>) ((data, length) => Marshal.Copy(this._frameData.Value3, 0, data, length)));
    }

    /// <summary>
    /// This method copies the frame's DepthImagePixel data to a pre-allocated byte array.
    /// 
    /// </summary>
    /// <param name="pixelData">The IntPtr of the byte array.</param><param name="pixelDataLength">The count of DepthImagePixels to copy to pixelData.  This must be equal to the frame’s PixelDataLength.</param>
    public unsafe void CopyDepthImagePixelDataTo(IntPtr pixelData, int pixelDataLength)
    {
      if (pixelData == IntPtr.Zero)
        throw new ArgumentNullException("pixelData");
      this.CopyPixelDataTo<IntPtr>(pixelData, pixelDataLength, (Action<IntPtr, int>) ((data, length) =>
      {
        fixed (DepthImagePixel* fixed_0 = this._frameData.Value4)
          Microsoft.Kinect.NativeMethods.CopyMemory(data, (IntPtr) ((void*) fixed_0), length * sizeof (DepthImagePixel));
      }));
    }

    /// <summary>
    /// This maps a depth coordinate to a color coordinate.
    /// 
    /// </summary>
    /// <param name="depthX">The X coordinate of the depth frame.</param><param name="depthY">The Y coordinate of the depth frame.</param><param name="colorImageFormat">The color format being used.</param>
    /// <returns>
    /// An ImagePoint that contains the X, Y locations in the color frame.
    /// </returns>
    [Obsolete("This method is replaced by Microsoft.Kinect.CoordinateMapper.MapDepthPointToColorPoint", false)]
    public ColorImagePoint MapToColorImagePoint(int depthX, int depthY, ColorImageFormat colorImageFormat)
    {
      if (colorImageFormat == ColorImageFormat.Undefined)
        throw new ArgumentException(Resources.ImageFormatNotSupported, "colorImageFormat");
      short depthPixelValue = (short) 0;
      if (depthX >= 0 && depthX < this.Width && (depthY >= 0 && depthY < this.Height))
        depthPixelValue = this._frameData.Value3[depthY * this.Width + depthX];
      return this._depthImageStream.Sensor.MapDepthToColorImagePoint(this.Format, depthX, depthY, depthPixelValue, colorImageFormat);
    }

    /// <summary>
    /// Looks up the depth frame coordinates for a given skeleton point.
    /// 
    /// </summary>
    /// <param name="skeletonPoint">The supplied skeleton point.</param>
    /// <returns>
    /// The ImagePoint that contains the X, Y and depth value of the given skeleton point.
    /// </returns>
    [Obsolete("This method is replaced by Microsoft.Kinect.CoordinateMapper.MapSkeletonPointToDepthPoint", false)]
    public DepthImagePoint MapFromSkeletonPoint(SkeletonPoint skeletonPoint)
    {
      return this._depthImageStream.Sensor.MapSkeletonPointToDepth(skeletonPoint, this.Format);
    }

    /// <summary>
    /// Looks up the skeleton point location of the given depth X, Y.
    /// 
    /// </summary>
    /// <param name="depthX">The X coordinate of the depth frame.</param><param name="depthY">The Y coordinate of the depth frame.</param>
    /// <returns>
    /// The skeleton point for the given X, Y.
    /// </returns>
    [Obsolete("This method is replaced by Microsoft.Kinect.CoordinateMapper.MapDepthPointToSkeletonPoint", false)]
    public SkeletonPoint MapToSkeletonPoint(int depthX, int depthY)
    {
      short depthPixelValue = (short) 0;
      if (depthX >= 0 && depthX < this.Width && (depthY >= 0 && depthY < this.Height))
        depthPixelValue = this._frameData.Value3[depthY * this.Width + depthX];
      return this._depthImageStream.Sensor.MapDepthToSkeletonPoint(this.Format, depthX, depthY, depthPixelValue);
    }

    /// <summary>
    /// Disposes the frame object.
    /// 
    /// </summary>
    /// <param name="disposing">Specify true to indicate that the class should clean up all resources.</param>
    protected override void Dispose(bool disposing)
    {
      if (!disposing)
        return;
      lock (this._dataAccessLock)
      {
        this._depthImageStream.UnlockPixelData(this._frameData);
        this._frameData = (DataPool<int, ImageType, ImageResolution, short[], DepthImagePixel[]>.Entry) null;
      }
    }
  }
}
