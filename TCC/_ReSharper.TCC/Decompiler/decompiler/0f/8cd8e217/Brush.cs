﻿// Type: System.Drawing.Brush
// Assembly: System.Drawing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
// Assembly location: C:\Windows\Microsoft.NET\Framework\v4.0.30319\System.Drawing.dll

using System;
using System.ComponentModel;
using System.Runtime;
using System.Runtime.InteropServices;

namespace System.Drawing
{
  /// <summary>
  /// Defines objects used to fill the interiors of graphical shapes such as rectangles, ellipses, pies, polygons, and paths.
  /// </summary>
  /// <filterpriority>1</filterpriority><completionlist cref="T:System.Drawing.Brushes"/>
  public abstract class Brush : MarshalByRefObject, ICloneable, IDisposable
  {
    private IntPtr nativeBrush;

    [Browsable(false)]
    [EditorBrowsable(EditorBrowsableState.Never)]
    internal IntPtr NativeBrush
    {
      get
      {
        return this.nativeBrush;
      }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="T:System.Drawing.Brush"/> class.
    /// </summary>
    [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
    protected Brush()
    {
    }

    ~Brush()
    {
      this.Dispose(false);
    }

    /// <summary>
    /// When overridden in a derived class, creates an exact copy of this <see cref="T:System.Drawing.Brush"/>.
    /// </summary>
    /// 
    /// <returns>
    /// The new <see cref="T:System.Drawing.Brush"/> that this method creates.
    /// </returns>
    /// <filterpriority>1</filterpriority>
    public abstract object Clone();

    /// <summary>
    /// In a derived class, sets a reference to a GDI+ brush object.
    /// </summary>
    /// <param name="brush">A pointer to the GDI+ brush object.</param>
    protected internal void SetNativeBrush(IntPtr brush)
    {
      System.Drawing.IntSecurity.UnmanagedCode.Demand();
      this.SetNativeBrushInternal(brush);
    }

    /// <summary>
    /// Releases all resources used by this <see cref="T:System.Drawing.Brush"/> object.
    /// </summary>
    /// <filterpriority>1</filterpriority><PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode"/></PermissionSet>
    public void Dispose()
    {
      this.Dispose(true);
      GC.SuppressFinalize((object) this);
    }

    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="T:System.Drawing.Brush"/> and optionally releases the managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected virtual void Dispose(bool disposing)
    {
      if (!(this.nativeBrush != IntPtr.Zero))
        return;
      try
      {
        SafeNativeMethods.Gdip.GdipDeleteBrush(new HandleRef((object) this, this.nativeBrush));
      }
      catch (Exception ex)
      {
        if (!System.Drawing.ClientUtils.IsSecurityOrCriticalException(ex))
          return;
        throw;
      }
      finally
      {
        this.nativeBrush = IntPtr.Zero;
      }
    }

    internal void SetNativeBrushInternal(IntPtr brush)
    {
      this.nativeBrush = brush;
    }
  }
}
