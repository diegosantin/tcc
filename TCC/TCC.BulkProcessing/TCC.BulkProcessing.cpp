#include "stdafx.h"
#include "TCC.BulkProcessing.h"

extern "C" TCCBULKPROCESSING_API MarshalData* BP_ProcessData(BYTE* colorPixels, int colorPixelsLength, short* depthData, int depthDataLength, BYTE* depthPixels)
{
	int* distances = GetDistances(depthData, depthDataLength);	
	GetDepthPixels(distances, depthDataLength, depthPixels);

	std::vector<Point*> contourPoints = GetContourPoints(distances, depthDataLength);

	DrawContour(colorPixels, contourPoints);

	std::vector<Point*> reducedPoints = GetReducedPoints(contourPoints, 10);
	
	DrawLines(colorPixels, reducedPoints);
	DrawReducedPoints(colorPixels, reducedPoints);

	MarshalData* marshalData = new MarshalData();
	marshalData->ContourCount = reducedPoints.size();

	for(size_t i = 0; i < reducedPoints.size(); ++i)
		marshalData->ContourPoints[i] = *reducedPoints.at(i);

	reducedPoints.clear();
	contourPoints.clear();
	delete distances;
	
	return marshalData;
}

int* GetDistances(short* depthData, int depthDataLength)
{
	int* distances = new int[depthDataLength];

	for (int i = 0; i < depthDataLength; ++i)
		distances[i] = depthData[i] >> 3;

	return distances;
}

void GetDepthPixels(int* distances, int length, BYTE* depthPixels)
{
	int pixelLength = WIDTH * HEIGHT * 4;
	for (int i = 0, colorIndex = 0; i < length && colorIndex < pixelLength; i++, colorIndex += 4)
	{
		if (distances[i] == -1)
		{
			depthPixels[colorIndex] = 212;
			depthPixels[colorIndex + 1] = 212;
			depthPixels[colorIndex + 2] = 212;
		}
		else if (distances[i] == 0)
		{
			depthPixels[colorIndex] = 47;
			depthPixels[colorIndex + 1] = 142;
			depthPixels[colorIndex + 2] = 47;
		}
		else if (distances[i] < 2000)
		{
			depthPixels[colorIndex] = 212;
			depthPixels[colorIndex + 1] = 212;
			depthPixels[colorIndex + 2] = 212;
		}
		else
		{
			depthPixels[colorIndex] = 0;
			depthPixels[colorIndex + 1] = 0;
			depthPixels[colorIndex + 2] = 0;
		}
	}
}

std::vector<Point*> GetContourPoints(int* distances, int length)
{
	bool* nearMatrix = GetNearMatrix(distances, length);
	return LocalizeContourPoints(nearMatrix);
}

std::vector<Point*> LocalizeContourPoints(bool* nearMatrix)
{
	bool* contour = new bool[WIDTH*HEIGHT];
	std::vector<Point*> contourPoints;

	for (int i = 1; i < HEIGHT - 1; ++i)
	{
		for (int j = 1; j < WIDTH - 1; ++j)
		{
			int index = WIDTH * i + j;
			if (nearMatrix[index])
			{
				int count = GetValidAdjacentPixelsCount(nearMatrix, i, j);

				Point* point = new Point();
				point->X = i;
				point->Y = j;

				if (count != 4)
				{
					contour[index] = true;
					contourPoints.push_back(point);
				}
			}
		}
	}

	for (size_t i = 0; i < contourPoints.size(); ++i)
	{
		if (contour[contourPoints.at(i)->X * WIDTH + contourPoints.at(i)->Y])
		{
			std::vector<Point*> currentContour = CalculateFrontier(nearMatrix, contourPoints.at(i), contour);

			int contourLength = currentContour.size();
			if (contourLength > 22 && contourLength / (contourPoints.size() * 1.0f) > 0.20f)
			{
				contourPoints = currentContour;
				break;
			}

			currentContour.clear();
		}
	}

	delete nearMatrix;
	delete contour;

	return contourPoints;
}

bool* GetNearMatrix(int* distances, int number)
{
	bool* nearMatrix = new bool[WIDTH*HEIGHT];

	int max = INT_MIN, min = INT_MAX;

	for (int i = 0; i < number; ++i)
	{
		if (distances[i] > max)
			max = distances[i];

		if (distances[i] < min && distances[i] != -1)
			min = distances[i];
	}

	const int margin = 300;
	for (int i = 0; i < HEIGHT; ++i)
	{
		for (int j = 0; j < WIDTH; ++j)
		{
			int index = i * WIDTH + j;
			nearMatrix[index] = distances[index] <= margin && distances[index] != -1;
		}
	}

	for (int i = 0; i < WIDTH; ++i)
		nearMatrix[i] = false;

	for (int i = 0; i < WIDTH; ++i)
		nearMatrix[479 * WIDTH + i] = false;

	for (int i = 0; i < HEIGHT; ++i)
		nearMatrix[i * WIDTH] = false;

	for (int i = 0; i < HEIGHT; ++i)
		nearMatrix[i * WIDTH + 639] = false;

	return nearMatrix;
}

void DrawContour(BYTE* colorPixels, std::vector<Point*> contourPoints)
{
	for (size_t i = 0; i < contourPoints.size(); ++i)
	{
		Point* currentPoint = contourPoints.at(i);
		int position = ((currentPoint->X - 1) * WIDTH * 4) + ((currentPoint->Y - 1) * 4);
		colorPixels[position] = 0;
		colorPixels[position+1] = 0;
		colorPixels[position+2] = 255;
	}
}

void DrawLines(BYTE* colorPixels, std::vector<Point*> reducedPoints)
{
	for (int i = 0; i < reducedPoints.size(); ++i)
	{
		if (i >= reducedPoints.size())
			break;

		Point* currentPoint = reducedPoints.at(i);
		Point* nextPoint;

		if (i + 1 < reducedPoints.size())
			nextPoint = reducedPoints.at(i+1);
		else
			nextPoint = reducedPoints.at(0);

		std::vector<Point*> linePoints = GetPointsOnLine(currentPoint->X, currentPoint->Y, nextPoint->X, nextPoint->Y);

		for (int j = 0; j < linePoints.size(); ++j)
		{
			Point* point = linePoints.at(j);
			int position = ((point->X - 1) * WIDTH * 4) + ((point->Y - 1) * 4);
			colorPixels[position] = 51;
			colorPixels[position + 1] = 255;
			colorPixels[position + 2] = 51;
		}
	}
}

void DrawReducedPoints(BYTE* colorPixels, std::vector<Point*> reducedPoints)
{
	for (int i = 0; i < reducedPoints.size(); ++i)
	{
		Point* point = reducedPoints.at(i);
		int topLeft = ((point->X - 2) * 640 * 4) + ((point->Y - 2) * 4);
		int topCenter = ((point->X - 1) * 640 * 4) + ((point->Y - 2) * 4);
		int topRight = ((point->X) * 640 * 4) + ((point->Y - 2) * 4);

		int middleLeft = ((point->X - 2) * 640 * 4) + ((point->Y - 1) * 4);
		int middleCenter = ((point->X - 1) * 640 * 4) + ((point->Y - 1) * 4);
		int middleRight = ((point->X) * 640 * 4) + ((point->Y - 1) * 4);

		int bottomLeft = ((point->X - 2) * 640 * 4) + ((point->Y) * 4);
		int bottomCenter = ((point->X - 1) * 640 * 4) + ((point->Y) * 4);
		int bottomRight = ((point->X) * 640 * 4) + ((point->Y) * 4);

		colorPixels[topLeft] = 0;
		colorPixels[topLeft + 1] = 0;
		colorPixels[topLeft + 2] = 0;

		colorPixels[topCenter] = 0;
		colorPixels[topCenter + 1] = 0;
		colorPixels[topCenter + 2] = 0;

		colorPixels[topRight] = 0;
		colorPixels[topRight + 1] = 0;
		colorPixels[topRight + 2] = 0;

		colorPixels[middleLeft] = 0;
		colorPixels[middleLeft + 1] = 0;
		colorPixels[middleLeft + 2] = 0;

		colorPixels[middleCenter] = 0;
		colorPixels[middleCenter + 1] = 0;
		colorPixels[middleCenter + 2] = 0;

		colorPixels[middleRight] = 0;
		colorPixels[middleRight + 1] = 0;
		colorPixels[middleRight + 2] = 0;

		colorPixels[bottomLeft] = 0;
		colorPixels[bottomLeft + 1] = 0;
		colorPixels[bottomLeft + 2] = 0;

		colorPixels[bottomCenter] = 0;
		colorPixels[bottomCenter + 1] = 0;
		colorPixels[bottomCenter + 2] = 0;

		colorPixels[bottomRight] = 0;
		colorPixels[bottomRight + 1] = 0;
		colorPixels[bottomRight + 2] = 0;
	}
}

std::vector<Point*> CalculateFrontier(bool* nearMatrix, Point* start, bool* contour)
{
	std::vector<Point*> points;
	Point* last = new Point();
	last->X = -1;
	last->Y = -1;

	Point* current = new Point();
	current->X = start->X;
	current->Y = start->Y;

	int dir = 0;

	do
	{
		if (nearMatrix[current->X * WIDTH + current->Y])
		{
			dir = (dir + 1) % 4;
			if (current->X != last->X || current->Y != last->Y)
			{
				Point* newPoint = new Point();
				newPoint->X = current->X;
				newPoint->Y = current->Y;

				points.push_back(newPoint);

				Point* newLastPoint = new Point();
				newLastPoint->X = current->X;
				newLastPoint->Y = current->Y;

				last = newLastPoint;
				contour[current->X * WIDTH + current->Y] = false;
			}
		}
		else 
		{
			dir = (dir + 4 - 1) % 4;
		}

		switch(dir)
		{
		case 0:
			current->X += 1;
			break;
		case 1:
			current->Y += 1;
			break;
		case 2:
			current->X -= 1;
			break;
		case 3:
			current->Y -= 1;
			break;
		}
	} while (current->X != start->X || current->Y != start->Y);

	return points;
}

std::vector<Point*> GetPointsOnLine(int x0, int y0, int x1, int y1)
{
	bool steep = abs(y1 - y0) > abs(x1 - x0);
	if (steep)
	{
		int temp = x0;
		x0 = y0;
		y0 = temp;

		temp = x1;
		x1 = y1;
		y1 = temp;
	}
	if (x0 > x1)
	{
		int temp = x0;
		x0 = x1;
		x1 = temp;

		temp = y0;
		y0 = y1;
		y1 = temp;
	}

	int dx = x1 - x0;
	int dy = abs(y1 - y0);
	int error = dx / 2;
	int ystep = (y0 < y1) ? 1 : -1;
	int y = y0;

	std::vector<Point*> points;

	for (int x = x0; x  <= x1; ++x)
	{
		Point* point = new Point();
		point->X = steep ? y : x;
		point->Y = steep ? x : y;
		points.push_back(point);
		error = error - dy;
		if (error < 0)
		{
			y += ystep;
			error += dx;
		}
	}

	return points;
}

int GetValidAdjacentPixelsCount(bool* nearMatrix, int i, int j)
{
	int count = 0;

	if (nearMatrix[(i+1) * WIDTH + j])
		++count;

	if (nearMatrix[(i-1) * WIDTH + j])
		++count;

	if (nearMatrix[i * WIDTH + (j+1)])
		++count;

	if (nearMatrix[i * WIDTH + (j-1)])
		++count;

	return count;
}