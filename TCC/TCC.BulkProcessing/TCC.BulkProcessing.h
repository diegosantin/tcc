// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the TCCBULKPROCESSING_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// TCCBULKPROCESSING_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef TCCBULKPROCESSING_EXPORTS
#define TCCBULKPROCESSING_API __declspec(dllexport)
#else
#define TCCBULKPROCESSING_API __declspec(dllimport)
#endif

#include "TCC.DouglasPeucker.h"

const int WIDTH = 640;
const int HEIGHT = 480;

extern "C" TCCBULKPROCESSING_API MarshalData* BP_ProcessData(BYTE* colorPixels, int colorPixelsLength, short* depthData, int depthDataLength, BYTE* depthPixels);

void GetDepthPixels(int* distances, int legth, BYTE* depthPixels);
int* GetDistances(short* depthData, int depthDataLength);
std::vector<Point*> GetContourPoints(int* distances, int length);
std::vector<Point*> LocalizeContourPoints(bool* nearMatrix);
void DrawContour(BYTE* colorPixels, std::vector<Point*> contourPoints);
void DrawLines(BYTE* colorPixels, std::vector<Point*> reducedPoints);
void DrawReducedPoints(BYTE* colorPixels, std::vector<Point*> reducedPoints);
bool* GetNearMatrix(int* distances, int number);
int GetValidAdjacentPixelsCount(bool* nearMatrix, int i, int j);
std::vector<Point*> CalculateFrontier(bool* nearMatrix, Point* start, bool* contour);
std::vector<Point*> GetPointsOnLine(int x0, int y0, int x1, int y1);
