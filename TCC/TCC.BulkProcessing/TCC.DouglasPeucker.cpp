#include "stdafx.h"
#include "TCC.DouglasPeucker.h"
#include <algorithm>

std::vector<Point*> GetReducedPoints(std::vector<Point*> contourPoints, double tolerance)
{
	std::vector<Point*> points;
	int number = contourPoints.size();

	if (number < 3)
	{
		for (int i = 0; i < number; ++i)
			points.push_back(contourPoints[i]);

		return points;
	}

	int firstPoint = 0;
	int lastPoint = number - 1;

	std::vector<int> pointsIndexes;

	pointsIndexes.push_back(firstPoint);
	pointsIndexes.push_back(lastPoint);

	while (contourPoints.at(firstPoint)->X == contourPoints.at(lastPoint)->X && contourPoints.at(firstPoint)->Y == contourPoints.at(lastPoint)->Y)
		--lastPoint;

	DouglasPeuckerReduction(contourPoints, firstPoint, lastPoint, tolerance, &pointsIndexes);

	std::sort(pointsIndexes.begin(), pointsIndexes.end());
	for (int i = 0; i < pointsIndexes.size(); ++i)
		points.push_back(contourPoints.at(pointsIndexes.at(i)));

	return points;
}

void DouglasPeuckerReduction(std::vector<Point*> contourPoints, int firstPoint, int lastPoint, double tolerance, std::vector<int>* pointsIndexes)
{
	double maxDistance = 0;
	int indexFarthest = 0;

	for (int index = firstPoint; index < lastPoint; ++index)
	{
		double distance = GetPerpendicularDistance(contourPoints[firstPoint], contourPoints[lastPoint], contourPoints[index]);
		if (distance > maxDistance)
		{
			maxDistance = distance;
			indexFarthest = index;
		}
	}

	if (maxDistance > tolerance && indexFarthest != 0)
	{
		pointsIndexes->push_back(indexFarthest);

		DouglasPeuckerReduction(contourPoints, firstPoint, indexFarthest, tolerance, pointsIndexes);
		DouglasPeuckerReduction(contourPoints, indexFarthest, lastPoint, tolerance, pointsIndexes);
	}
}

double GetPerpendicularDistance(Point* pointA, Point* pointB, Point* pointC)
{
	double area = abs(0.5 * (pointA->X * pointB->Y + pointB->X * pointC->Y + pointC->X * pointA->Y - pointB->X * pointA->Y - pointC->X * pointB->Y - pointA->X * pointC->Y));
	double bottom = sqrt(pow(pointA->X - pointB->X, 2) + pow(pointA->Y - pointB->Y, 2));
	return area / bottom * 2;
}