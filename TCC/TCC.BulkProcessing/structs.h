#include "stdafx.h"

struct Point
{
public:
	int X;
	int Y;
};

struct Contour
{
public:
	std::vector<Point*> Points;
};

struct MarshalData
{
public:
	Point ContourPoints[2000];
	int ContourCount;
	Point Palm;
};

struct Hand
{
	Hand::Hand()
	{
		LeftUpperCorner = new Point();
		LeftUpperCorner->X = INT_MAX;
		LeftUpperCorner->Y = INT_MAX;

		RightDownCorner = new Point();
		RightDownCorner->X = INT_MIN;
		RightDownCorner->Y = INT_MIN;
	}

public:
	Point* Palm;
	Contour* Contour;
	std::vector<Point*> Inside;
	Point* LeftUpperCorner;
	Point* RightDownCorner;
};