#include "structs.h"

std::vector<Point*> GetReducedPoints(std::vector<Point*> contourPoints, double tolerance);
void DouglasPeuckerReduction(std::vector<Point*> contourPoints, int firstPoint, int lastPoint, double tolerance, std::vector<int>* pointsIndexes);
double GetPerpendicularDistance(Point* pointA, Point* pointB, Point* pointC); 