// TCC.DataProcessing.cpp : Defines the exported functions for the DLL application.

#include "stdafx.h"
#include "TCC.DataProcessing.h"
#include "math.h"

CTCCDataProcessing::CTCCDataProcessing()
{
	return;
}

extern "C" TCCDATAPROCESSING_API BYTE* DP_GetDistances( int* depthFrame )
{
	return NULL;
}

extern "C" TCCDATAPROCESSING_API int* DP_GetDepthPixels(int* distances[], int width, int height)
{
	return NULL;
}

extern "C" TCCDATAPROCESSING_API void* DP_GetContourPoints(int* distances[], int width, int height)
{
	return NULL;
}

extern "C" TCCDATAPROCESSING_API void DP_ProcessColorPixels(int* colorPixels[], void* contourPoints)
{
}