// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the TCCDATAPROCESSING_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// TCCDATAPROCESSING_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef TCCDATAPROCESSING_EXPORTS
#define TCCDATAPROCESSING_API __declspec(dllexport)
#else
#define TCCDATAPROCESSING_API __declspec(dllimport)
#endif

// This class is exported from the TCC.DataProcessing.dll
class TCCDATAPROCESSING_API CTCCDataProcessing {
public:
	CTCCDataProcessing(void);
};

extern "C" TCCDATAPROCESSING_API BYTE* DP_GetDistances(int* depthFrame);
extern "C" TCCDATAPROCESSING_API BYTE* DP_GetDepthPixels(int* distances, int width, int height);
extern "C" TCCDATAPROCESSING_API void* DP_GetContourPoints(int* distances, int width, int height);
extern "C" TCCDATAPROCESSING_API void DP_ProcessColorPixels(int* colorPixels, void* contourPoints);