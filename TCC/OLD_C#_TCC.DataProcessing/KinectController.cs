using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Kinect;

namespace TCC.DataProcessing
{
    public delegate void ColorFrameReady(byte[] pixels);
    public delegate void DepthFrameReady(byte[] pixels);

    public delegate void ContourPointsReady(IList<PointFT> points);

    public class KinectController
    {
        [DllImport("TCC.BulkProcessing.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern void BP_GetDistances(short[] data, int number, int[] distances);

        [DllImport("TCC.BulkProcessing.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern void BP_GetDepthPixels(int[] distances, int number, byte[] depthPixels);

        private const int Blue = 0;
        private const int Green = 1;
        private const int Red = 2;
        private KinectSensor sensor;

        public ColorFrameReady ColorFrameReady;
        public DepthFrameReady DepthFrameReady;
        public ContourPointsReady ContourPointsReady;

        public bool IsKinectConnected()
        {
            return KinectSensor.KinectSensors.Any();
        }

        public void InitializeSensor()
        {
            sensor = KinectSensor.KinectSensors.First();
            sensor.Start();
            sensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
            sensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
            sensor.SkeletonStream.Enable();
        }

        public void SearchTarget()
        {
            // alterar ElevationAngle para buscar player
        }

        public void StartRecognition()
        {
            if (ColorFrameReady != null && DepthFrameReady != null && ContourPointsReady != null)
                sensor.AllFramesReady += (sender, args) => ProcessImageFrames(args);
        }

        public void Exit()
        {
            sensor.Stop();
            sensor = null;
        }

        private void ProcessImageFrames(AllFramesReadyEventArgs args)
        {
            using (var colorFrame = args.OpenColorImageFrame())
            {
                if (colorFrame == null)
                    return;

                var colorPixels = new byte[colorFrame.PixelDataLength];
                colorFrame.CopyPixelDataTo(colorPixels);

                using (var depthFrame = args.OpenDepthImageFrame())
                {
                    if (depthFrame == null)
                        return;

                    var depth = new short[depthFrame.PixelDataLength];
                    depthFrame.CopyPixelDataTo(depth);

                    var distances = new int[depth.Length];
                    BP_GetDistances(depth, depth.Length, distances);

                    var depthWidth = depthFrame.Width;
                    var depthHeight = depthFrame.Height;

                    var depthPixels = new byte[depthHeight * depthWidth * 4];
                    BP_GetDepthPixels(distances, distances.Length, depthPixels);

                    DepthFrameReady(depthPixels);

                    var armsContourPoints = GetArmContourPoints(depthFrame, distances);

                    for (int i = 0; i < armsContourPoints.Count; i++)
                    {
                        var point = armsContourPoints[i];
                        var position = ((point.X - 1) * depthWidth * 4) + ((point.Y - 1) * 4);
                        colorPixels[position + Blue] = 0;
                        colorPixels[position + Green] = 0;
                        colorPixels[position + Red] = 255;
                    }

                    IList<PointFT> pointsReducedByDouglas = DouglasPeuckerHelper.DouglasPeuckerReduction(armsContourPoints, 10);
                    ContourPointsReady(pointsReducedByDouglas);

                    for (int i = 0; i < pointsReducedByDouglas.Count; i++)
                    {
                        if (i >= pointsReducedByDouglas.Count)
                            break;

                        var currentPoint = pointsReducedByDouglas[i];
                        PointFT nextPoint;

                        if (i + 1 < pointsReducedByDouglas.Count)
                            nextPoint = pointsReducedByDouglas[i + 1];
                        else
                            nextPoint = pointsReducedByDouglas[0];

                        var points = BresenhamHelper.GetPointsOnLine(currentPoint.X, currentPoint.Y, nextPoint.X, nextPoint.Y).ToList();

                        foreach (var point in points)
                        {
                            var position = ((point.X - 1) * depthFrame.Width * 4) + ((point.Y - 1) * 4);
                            colorPixels[position + Blue] = 51;
                            colorPixels[position + Green] = 255;
                            colorPixels[position + Red] = 51;
                        }

                        i++;
                    }

                    foreach(var point in pointsReducedByDouglas)
                    {
                        var topLeft = ((point.X - 2) * depthFrame.Width * 4) + ((point.Y - 2) * 4);
                        var topCenter = ((point.X - 1) * depthFrame.Width * 4) + ((point.Y - 2) * 4);
                        var topRight = ((point.X) * depthFrame.Width * 4) + ((point.Y - 2) * 4);

                        var middleLeft = ((point.X - 2) * depthFrame.Width * 4) + ((point.Y - 1) * 4);
                        var middleCenter = ((point.X - 1) * depthFrame.Width * 4) + ((point.Y - 1) * 4);
                        var middleRight = ((point.X) * depthFrame.Width * 4) + ((point.Y - 1) * 4);

                        var bottomLeft = ((point.X - 2) * depthFrame.Width * 4) + ((point.Y) * 4);
                        var bottomCenter = ((point.X - 1) * depthFrame.Width * 4) + ((point.Y) * 4);
                        var bottomRight = ((point.X) * depthFrame.Width * 4) + ((point.Y) * 4);

                        colorPixels[topLeft + Blue] = 0;
                        colorPixels[topLeft + Green] = 0;
                        colorPixels[topLeft + Red] = 0;

                        colorPixels[topCenter + Blue] = 0;
                        colorPixels[topCenter + Green] = 0;
                        colorPixels[topCenter + Red] = 0;

                        colorPixels[topRight + Blue] = 0;
                        colorPixels[topRight + Green] = 0;
                        colorPixels[topRight + Red] = 0;

                        colorPixels[middleLeft + Blue] = 0;
                        colorPixels[middleLeft + Green] = 0;
                        colorPixels[middleLeft + Red] = 0;

                        colorPixels[middleCenter + Blue] = 0;
                        colorPixels[middleCenter + Green] = 0;
                        colorPixels[middleCenter + Red] = 0;

                        colorPixels[middleRight + Blue] = 0;
                        colorPixels[middleRight + Green] = 0;
                        colorPixels[middleRight + Red] = 0;

                        colorPixels[bottomLeft + Blue] = 0;
                        colorPixels[bottomLeft + Green] = 0;
                        colorPixels[bottomLeft + Red] = 0;

                        colorPixels[bottomCenter + Blue] = 0;
                        colorPixels[bottomCenter + Green] = 0;
                        colorPixels[bottomCenter + Red] = 0;

                        colorPixels[bottomRight + Blue] = 0;
                        colorPixels[bottomRight + Green] = 0;
                        colorPixels[bottomRight + Red] = 0;
                    }
                    
                }

                ColorFrameReady(colorPixels);
            }
        }

        private IList<PointFT> GetArmContourPoints(DepthImageFrame depthFrame, int[] distances)
        {
            var valid = GenerateValidMatrix(depthFrame, distances);
            var hands = LocalizeHands(ref valid).ToList();
            return FilterContour(hands).ToList();
        }

        private IEnumerable<PointFT> FilterContour(List<Hand> hands)
        {
            if (!hands.Any())
                return new List<PointFT>();

            return hands.First().Contour;
        }

        private bool[][] GenerateValidMatrix(DepthImageFrame frame, int[] distance)
        {
            var near = new bool[frame.Height][];
            for (int i = 0; i < near.Length; ++i)
            {
                near[i] = new bool[frame.Width];
            }

            // Calculate max and min distance
            int max = int.MinValue, min = int.MaxValue;

            for (int k = 0; k < distance.Length; ++k)
            {
                if (distance[k] > max)
                    max = distance[k];
                if (distance[k] < min && distance[k] != -1)
                    min = distance[k];
            }

            // Decide if it is near or not
            const int margin = 700;
            for (int i = 0; i < near.Length; ++i)
            {
                for (int j = 0; j < near[i].Length; ++j)
                {
                    int index = frame.Width * i + j;
                    near[i][j] = distance[index] <= margin && distance[index] != -1;
                }
            }

            // Mark as not valid the borders of the matrix to improve the efficiency in some methods
            // First row
            for (int j = 0; j < near[0].Length; ++j)
                near[0][j] = false;

            // Last row
            var m = near.Length - 1;
            for (int j = 0; j < near[0].Length; ++j)
                near[m][j] = false;

            // First column
            for (int i = 0; i < near.Length; ++i)
                near[i][0] = false;

            // Last column
            m = near[0].Length - 1;
            for (int index = 0; index < near.Length; index++)
            {
                bool[] t = near[index];
                t[m] = false;
            }

            return near;
        }

        private IEnumerable<Hand> LocalizeHands(ref bool[][] valid)
        {
            int i, j, k;

            var hands = new List<Hand>();

            var contourPoints = new List<PointFT>();
            var insidePoints = new List<PointFT>();

            var contour = new bool[valid.Length][];
            for (i = 0; i < valid.Length; ++i)
            {
                contour[i] = new bool[valid[0].Length];
            }

            // Divide points in contour and inside points
            for (i = 1; i < valid.Length - 1; ++i)
            {
                for (j = 1; j < valid[i].Length - 1; ++j)
                {
                    if (valid[i][j])
                    {
                        // Count the number of valid adjacent points
                        var count = GetValidAdjacentPixelsCount(ref i, ref j, ref valid);

                        if (count == 4) // Inside
                        {
                            insidePoints.Add(new PointFT(i, j));
                        }
                        else // Contour
                        {
                            contour[i][j] = true;
                            contourPoints.Add(new PointFT(i, j));
                        }
                    }
                }
            }

            // Create the sorted contour list, using the turtle algorithm
            for (i = 0; i < contourPoints.Count; ++i)
            {
                var hand = new Hand();

                // If it is a possible start point
                if (contour[contourPoints[i].X][contourPoints[i].Y])
                {
                    // Calculate the contour
                    hand.Contour = CalculateFrontier(ref valid, contourPoints[i], ref contour);

                    // Check if the contour is big enough to be a hand
                    if (hand.Contour.Count / (contourPoints.Count * 1.0f) > 0.20f && hand.Contour.Count > 22)
                    {
                        // Calculate the container box
                        hand.CalculateContainerBox(5f);

                        // Add the hand to the list
                        hands.Add(hand);
                    }

                    // Don't look for more hands, if we reach the limit
                    if (hands.Count >= 2)
                    {
                        break;
                    }
                }
            }

            return hands;
        }

        private List<PointFT> CalculateFrontier(ref bool[][] valid, PointFT start, ref bool[][] contour)
        {
            var list = new List<PointFT>();
            var last = new PointFT(-1, -1);
            var current = new PointFT(start);
            int dir = 0;

            do
            {
                if (valid[current.X][current.Y])
                {
                    dir = (dir + 1) % 4;
                    if (current != last)
                    {
                        list.Add(new PointFT(current.X, current.Y));
                        last = new PointFT(current);
                        contour[current.X][current.Y] = false;
                    }
                }
                else
                {
                    dir = (dir + 4 - 1) % 4;
                }

                switch (dir)
                {
                    case 0:
                        current.X += 1;
                        break; // Down
                    case 1:
                        current.Y += 1;
                        break; // Right
                    case 2:
                        current.X -= 1;
                        break; // Up
                    case 3:
                        current.Y -= 1;
                        break; // Left
                }
            } while (current != start);

            return list;
        }

        private int GetValidAdjacentPixelsCount(ref int i, ref int j, ref bool[][] valid)
        {
            var count = 0;

            if (valid[i + 1][j])
                ++count;

            if (valid[i - 1][j])
                ++count;

            if (valid[i][j + 1])
                ++count;

            if (valid[i][j - 1])
                ++count;

            return count;
        }
    }
}