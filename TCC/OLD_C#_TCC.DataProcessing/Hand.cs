using System.Collections.Generic;

namespace TCC.DataProcessing
{
    public class Hand
    {
        public Vector3FT Palm3D;

        public Hand()
        {
            Palm = new PointFT(-1, -1);

            Fingertips = new List<PointFT>(5);
            Fingertips3D = new List<Vector3FT>(5);

            Contour = new List<PointFT>(2000);
            Inside = new List<PointFT>(6000);

            LeftUpperCorner = new PointFT(int.MaxValue, int.MaxValue);
            RightDownCorner = new PointFT(int.MinValue, int.MinValue);
        }

        public PointFT Palm { get; set; }
        public List<PointFT> Fingertips { get; set; }

        public List<Vector3FT> Fingertips3D { get; set; }

        public List<PointFT> Contour { get; set; }
        public List<PointFT> Inside { get; set; }

        public PointFT LeftUpperCorner { get; set; }
        public PointFT RightDownCorner { get; set; }

        // Check if a point is inside the hand countour box
        public bool IsPointInsideContainerBox(PointFT p)
        {
            return p.X < RightDownCorner.X && p.X > LeftUpperCorner.X && p.Y > LeftUpperCorner.Y && p.Y < RightDownCorner.Y;
        }

        public bool IsCircleInsideContainerBox(PointFT p, float r)
        {
            if (LeftUpperCorner.X > p.X - r)
            {
                return false;
            }
            if (RightDownCorner.X < p.X + r)
            {
                return false;
            }
            if (LeftUpperCorner.Y > p.Y - r)
            {
                return false;
            }
            if (RightDownCorner.Y < p.Y + r)
            {
                return false;
            }

            return true;
        }

        // Calculate the contour box of the hand if it possible
        public bool CalculateContainerBox(float reduction = 0)
        {
            if (Contour != null && Contour.Count > 0)
            {
                for (int j = 0; j < Contour.Count; ++j)
                {
                    if (LeftUpperCorner.X > Contour[j].X)
                        LeftUpperCorner.X = Contour[j].X;

                    if (RightDownCorner.X < Contour[j].X)
                        RightDownCorner.X = Contour[j].X;

                    if (LeftUpperCorner.Y > Contour[j].Y)
                        LeftUpperCorner.Y = Contour[j].Y;

                    if (RightDownCorner.Y < Contour[j].Y)
                        RightDownCorner.Y = Contour[j].Y;
                }

                var incX = (int)((RightDownCorner.X - LeftUpperCorner.X) * 30f);
                var incY = (int)((RightDownCorner.Y - LeftUpperCorner.Y) * 30f);
                var inc = new PointFT(incX, incY);
                LeftUpperCorner = LeftUpperCorner + inc;
                RightDownCorner = RightDownCorner + inc;

                return true;
            }

            return false;
        }

        // Check if the hand is open
        public bool IsOpen()
        {
            return Fingertips.Count == 5;
        }

        // Check if the hand is close
        public bool IsClose()
        {
            return Fingertips.Count == 0;
        }

        // Obtain the 3D normalized point and add it to the list of fingertips
        public void Calculate3DPoints(int width, int height, int[] distance)
        {
            if (Palm.X != -1 && Palm.Y != -1)
                Palm3D = transformTo3DCoord(Palm, width, height, distance[Palm.X * width + Palm.Y]);

            Fingertips3D.Clear();
            for (int i = 0; i < Fingertips.Count; ++i)
            {
                PointFT f = Fingertips[i];
                Fingertips3D.Add(transformTo3DCoord(f, width, height, distance[f.X * width + f.Y]));
            }
        }

        // Normalize in the interval [-1, 1] the given 3D point.
        // The Z value is in the inverval [-1, 0], being 0 the closest distance.
        private Vector3FT transformTo3DCoord(PointFT p, int width, int height, int distance)
        {
            var v = new Vector3FT();
            v.X = p.Y / (width * 1.0f) * 2 - 1;
            v.Y = (1 - p.X / (height * 1.0f)) * 2 - 1;
            v.Z = (distance - 400) / -7600.0f;
            return v;
        }
    }
}