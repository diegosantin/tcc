using System;
using System.Collections.Generic;

namespace TCC.DataProcessing
{
    public static class DouglasPeuckerHelper
    {
        /// <summary>
        /// Uses the Douglas Peucker algorithm to reduce the number of PointFTs.
        /// </summary>
        /// <param name="pointFTs">The PointFTs.</param>
        /// <param name="tolerance">The tolerance.</param>
        /// <returns></returns>
        public static IList<PointFT> DouglasPeuckerReduction(IList<PointFT> pointFTs, Double tolerance)
        {
            if (pointFTs == null || pointFTs.Count < 3)
                return pointFTs;

            const int firstPointFT = 0;
            var lastPointFT = pointFTs.Count - 1;
            var pointsIndexesToKeep = new List<Int32>();

            //Add the first and last index to the keepers
            pointsIndexesToKeep.Add(firstPointFT);
            pointsIndexesToKeep.Add(lastPointFT);

            //The first and the last PointFT cannot be the same
            while (pointFTs[firstPointFT].Equals(pointFTs[lastPointFT]))
            {
                lastPointFT--;
            }

            DouglasPeuckerReduction(pointFTs, firstPointFT, lastPointFT, tolerance, ref pointsIndexesToKeep);

            var returnPoints = new List<PointFT>();
            pointsIndexesToKeep.Sort();
            foreach (Int32 index in pointsIndexesToKeep)
            {
                returnPoints.Add(pointFTs[index]);
            }

            return returnPoints;
        }

        /// <summary>
        /// Douglases the peucker reduction.
        /// </summary>
        /// <param name="pointFTs">The PointFTs.</param>
        /// <param name="firstPointFT">The first PointFT.</param>
        /// <param name="lastPointFT">The last PointFT.</param>
        /// <param name="tolerance">The tolerance.</param>
        /// <param name="pointFTIndexsToKeep">The PointFT index to keep.</param>
        private static void DouglasPeuckerReduction(IList<PointFT> pointFTs, Int32 firstPointFT, Int32 lastPointFT, Double tolerance, ref List<Int32> pointFTIndexsToKeep)
        {
            Double maxDistance = 0;
            Int32 indexFarthest = 0;

            for (Int32 index = firstPointFT; index < lastPointFT; index++)
            {
                Double distance = PerpendicularDistance(pointFTs[firstPointFT], pointFTs[lastPointFT], pointFTs[index]);
                if (distance > maxDistance)
                {
                    maxDistance = distance;
                    indexFarthest = index;
                }
            }
                
            if (maxDistance > tolerance && indexFarthest != 0)
            {
                //Add the largest PointFT that exceeds the tolerance
                pointFTIndexsToKeep.Add(indexFarthest);

                DouglasPeuckerReduction(pointFTs, firstPointFT, indexFarthest, tolerance, ref pointFTIndexsToKeep);
                DouglasPeuckerReduction(pointFTs, indexFarthest, lastPointFT, tolerance, ref pointFTIndexsToKeep);
            }
        }

        public static Double PerpendicularDistance(PointFT pointFT1, PointFT pointFT2, PointFT pointFT)
        {
            var area = Math.Abs(.5 * (pointFT1.X * pointFT2.Y + pointFT2.X * pointFT.Y + pointFT.X * pointFT1.Y - pointFT2.X * pointFT1.Y - pointFT.X * pointFT2.Y - pointFT1.X * pointFT.Y));

            var bottom = Math.Sqrt(Math.Pow(pointFT1.X - pointFT2.X, 2) + Math.Pow(pointFT1.Y - pointFT2.Y, 2));
            var height = area / bottom * 2;

            return height;
        }
    }
}