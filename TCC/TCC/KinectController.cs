using System;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Kinect;

namespace TCC
{
    public delegate void ColorFrameReady(byte[] pixels);
    public delegate void DepthFrameReady(byte[] pixels);
    public delegate void ContourPointsReady(Point[] points);

    public class KinectController
    {
        [DllImport("TCC.BulkProcessing.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr BP_ProcessData(byte[] colorPixels, int colorPixelsLength, short[] depthData, int depthDataLength, byte[] depthPixels);

        private KinectSensor sensor;

        public ColorFrameReady ColorFrameReady;
        public DepthFrameReady DepthFrameReady;
        public ContourPointsReady ContourPointsReady;

        public bool IsKinectConnected()
        {
            return KinectSensor.KinectSensors.Any(x => x.Status == KinectStatus.Connected);
        }

        public void InitializeSensor()
        {
            sensor = KinectSensor.KinectSensors.First();
            sensor.Start();
            sensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
            sensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
            sensor.SkeletonStream.Enable();
        }

        public void StartRecognition()
        {
            if (ColorFrameReady != null && DepthFrameReady != null && ContourPointsReady != null)
                sensor.AllFramesReady += (sender, args) => ProcessImageFrames(args);
        }

        public void Exit()
        {
            sensor.Stop();
            sensor = null;
        }

        private void ProcessImageFrames(AllFramesReadyEventArgs args)
        {
            using (var colorFrame = args.OpenColorImageFrame())
            {
                if (colorFrame == null)
                    return;

                var colorPixels = new byte[colorFrame.PixelDataLength];
                colorFrame.CopyPixelDataTo(colorPixels);

                using (var depthFrame = args.OpenDepthImageFrame())
                {
                    if (depthFrame == null)
                        return;

                    var depthData = new short[depthFrame.PixelDataLength];
                    depthFrame.CopyPixelDataTo(depthData);

                    var depthPixels = new byte[depthFrame.Width * depthFrame.Height * 4];

                    BP_ProcessData(colorPixels, colorPixels.Length, depthData, depthData.Length, depthPixels);
                    var processedPtr = BP_ProcessData(colorPixels, colorPixels.Length, depthData, depthData.Length, depthPixels);
                    var processedContour = (Contour) Marshal.PtrToStructure(processedPtr, typeof (Contour));
                    var contourPoints = processedContour.ContourPoints.Take(processedContour.ContourCount).ToArray();

                    DepthFrameReady(depthPixels);
                    ContourPointsReady(contourPoints);
                    ColorFrameReady(colorPixels);

                    depthPixels = null;
                    colorPixels = null;

                    Marshal.FreeHGlobal(processedPtr);
                }
            }
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Contour
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2000)]
        public Point[] ContourPoints;
        
        public int ContourCount;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Point
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}