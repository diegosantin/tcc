﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace TCC
{
    public partial class MainWindow
    {
        private readonly KinectController kinectControllerBaca;
        private readonly WriteableBitmap personImage;
        private readonly WriteableBitmap depthImage;
        private const int FrameWidth = 640;
        private const int FrameHeight = 480;

        public MainWindow()
        {
            Closed += MainWindowClosed;
            InitializeComponent();
            kinectControllerBaca = new KinectController();

            PersonImage.Source = personImage = new WriteableBitmap(FrameWidth, FrameHeight, 96, 96, PixelFormats.Bgr32, null);
            DepthImage.Source = depthImage = new WriteableBitmap(FrameWidth, FrameHeight, 96, 96, PixelFormats.Bgr32, null);

            InitializeSensor();
        }

        public void InitializeSensor()
        {
            if (kinectControllerBaca.IsKinectConnected())
            {
                kinectControllerBaca.InitializeSensor();

                kinectControllerBaca.ColorFrameReady += pixels =>
                                                        {
                                                            var rectangle = new Int32Rect(0, 0, 640, 480);
                                                            personImage.WritePixels(rectangle, pixels, FrameWidth * 4, 0);
                                                        };

                kinectControllerBaca.DepthFrameReady += pixels =>
                                                        {
                                                            var rectangle = new Int32Rect(0, 0, FrameWidth, FrameHeight);
                                                            depthImage.WritePixels(rectangle, pixels, FrameWidth * 4, 0);
                                                        };

                kinectControllerBaca.ContourPointsReady += points =>
                                                           {
                                                               
                                                           };;

                kinectControllerBaca.StartRecognition();
            }
        }

        private void DrawSymbol(char symbol)
        {
            
        }

        void MainWindowClosed(object sender, EventArgs e)
        {
            kinectControllerBaca.Exit();
        }
    }
}
