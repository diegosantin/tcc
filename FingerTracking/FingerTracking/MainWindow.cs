﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FingerTracking
{
    public partial class MainWindow : Form
    {
        private readonly KinectTracker kinectController;

        public MainWindow()
        {
            Task.Factory.StartNew(InitializeComponent);
            kinectController = new KinectTracker();

            kinectController.SetEventColorReady(DrawColorImage);

            if (kinectController.isConnected())
            {
                kinectController.Start();

                // Show the default values of the Kinect settings
                double initialTheta = kinectController.Settings.Theta/(Math.PI/180);
                thetaTextBox.Text = initialTheta.ToString();
                thetaTrackBar.Value = (int) initialTheta;

                int initialK = kinectController.Settings.K;
                kTextBox.Text = initialK.ToString();
                kTrackBar.Value = initialK;

                float initialNearSpace = kinectController.Settings.NearSpacePerc;
                NearSpaceTextBox.Text = initialNearSpace.ToString();
                NearSpaceTrackBar.Value = (int) (initialNearSpace*100);

                textMarginLeft.Text = kinectController.Settings.MarginLeftPerc.ToString();
                textMarginRight.Text = kinectController.Settings.MarginRightPerc.ToString();
                textMarginTop.Text = kinectController.Settings.MarginTopPerc.ToString();
                textMarginBot.Text = kinectController.Settings.MarginBotPerc.ToString();

                smoothTextBox.Text = kinectController.Settings.SmoothingIterations.ToString();
                smoothTrackBar.Value = kinectController.Settings.SmoothingIterations;

                boxReductionTextBox.Text = ((int) (kinectController.Settings.ContainerBoxReduction*100)).ToString();
                boxReductionTrackBar.Value = (int) (kinectController.Settings.ContainerBoxReduction*100);
            }
            else
            {
                // Show an error
                Console.WriteLine("There is not any Kinect device connected.\nConnect it and restart the application.\n");
            }
        }

        // Show the color image in both image elements
        private void DrawColorImage()
        {
            colorImage.Image = kinectController.getColorImage();
            trackingImage.Image = kinectController.getColorImage();
        }

        // Show the color and tracked images in the image elements
        private void DrawDepthImage()
        {
            colorImage.Image = kinectController.getColorImage();
            trackingImage.Image = kinectController.getDepthImage();
        }

        private void ColorButtonClick(object sender, EventArgs e)
        {
            // Do nothing after the depth image is ready
            kinectController.ClearEventDepthReady();

            // Show the image after the color image is ready
            KinectTracker.AfterReady a = DrawColorImage;
            kinectController.SetEventColorReady(a);
        }

        private void DepthButtonClick(object sender, EventArgs e)
        {
            // Do nothing after the Camera color image is ready
            kinectController.ClearEventColorReady();

            // After the depth image is ready and the tracking done
            KinectTracker.AfterReady a = DrawDepthImage; // Show the depth image
            a = a + ShowNumberFingers; // Show the number of fingers
            kinectController.SetEventDepthReady(a);
        }

        private void ThetaTrackBarScroll(object sender, EventArgs e)
        {
            kinectController.Settings.Theta = thetaTrackBar.Value*(Math.PI/180);
            thetaTextBox.Text = thetaTrackBar.Value.ToString();
        }

        private void KTrackBarScroll(object sender, EventArgs e)
        {
            kinectController.Settings.K = kTrackBar.Value;
            kTextBox.Text = kTrackBar.Value.ToString();
        }

        private void ShowNumberFingers()
        {
            if (kinectController.Hands.Count > 0)
            {
                if (kinectController.Hands.Count > 0)
                    fingersTextBox1.Text = kinectController.Hands[0].Fingertips.Count.ToString();
                if (kinectController.Hands.Count > 1)
                    fingersTextBox2.Text = kinectController.Hands[1].Fingertips.Count.ToString();
            }
        }

        private void NearSpaceTrackBarScroll(object sender, EventArgs e)
        {
            if (AbsoluteCheckBox.Checked)
            {
                kinectController.Settings.AbsoluteSpace = NearSpaceTrackBar.Value*50;
                NearSpaceTextBox.Text = kinectController.Settings.AbsoluteSpace.ToString();
            }
            else
            {
                kinectController.Settings.AbsoluteSpace = -1;
                kinectController.Settings.NearSpacePerc = NearSpaceTrackBar.Value/100.0f;
                NearSpaceTextBox.Text = kinectController.Settings.NearSpacePerc.ToString();
            }
        }

        private void SmoothScroll(object sender, EventArgs e)
        {
            kinectController.Settings.SmoothingIterations = smoothTrackBar.Value;
            smoothTextBox.Text = smoothTrackBar.Value.ToString();
        }

        private void MarginsChanged(object sender, EventArgs e)
        {
            var box = (TextBox) sender;
            float perc = 0;
            if (!box.Text.Equals(""))
            {
                try
                {
                    perc = Convert.ToSingle(box.Text);
                }
                catch (Exception exception)
                {
                    perc = 0;
                }
            }
            perc = Math.Min(perc, 30);
            String percStr = perc.ToString();

            if (checkSameMargins.Checked)
            {
                kinectController.Settings.MarginLeftPerc = perc;
                textMarginLeft.Text = percStr;
                kinectController.Settings.MarginRightPerc = perc;
                textMarginRight.Text = percStr;
                kinectController.Settings.MarginTopPerc = perc;
                textMarginTop.Text = percStr;
                kinectController.Settings.MarginBotPerc = perc;
                textMarginBot.Text = percStr;
            }
            else
            {
                if (box.Name.Equals("textMarginLeft"))
                {
                    kinectController.Settings.MarginLeftPerc = perc;
                    textMarginLeft.Text = percStr;
                }
                else if (box.Name.Equals("textMarginRight"))
                {
                    kinectController.Settings.MarginRightPerc = perc;
                    textMarginRight.Text = percStr;
                }
                else if (box.Name.Equals("textMarginTop"))
                {
                    kinectController.Settings.MarginTopPerc = perc;
                    textMarginTop.Text = percStr;
                }
                else if (box.Name.Equals("textMarginBot"))
                {
                    kinectController.Settings.MarginBotPerc = perc;
                    textMarginBot.Text = percStr;
                }
            }
        }

        ~MainWindow()
        {
            kinectController.Stop();
        }

        private void colorImage_Click(object sender, EventArgs e)
        {

        }
    }
}