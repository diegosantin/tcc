﻿using System;
using Microsoft.Kinect;

namespace FingerTracking
{
    public class KinectSettings
    {
        public KinectSettings()
        {
            SetDefault();
        }

        public DepthImageFormat DepthFormat { get; set; }

        public float MarginLeftPerc { get; set; }
        public float MarginRightPerc { get; set; }
        public float MarginTopPerc { get; set; }
        public float MarginBotPerc { get; set; }

        public float NearSpacePerc { get; set; }
        public int AbsoluteSpace { get; set; }

        public int FindCenterContourJump { get; set; }
        public int FindCenterInsideJump { get; set; }
        public double FingertipFindJumpPerc { get; set; }

        public int FindFingertipsJump { get; set; }
        public double Theta { get; set; }
        public int K { get; set; }

        public int MaxTrackedHands { get; set; }

        public int SmoothingIterations { get; set; }

        public float ContainerBoxReduction { get; set; }

        public int ScreenSize
        {
            get
            {
                if (DepthFormat == DepthImageFormat.Resolution80x60Fps30) return 4800;
                if (DepthFormat == DepthImageFormat.Resolution320x240Fps30) return 76800;
                if (DepthFormat == DepthImageFormat.Resolution640x480Fps30) return 307200;
                return -1;
            }
        }

        public int ScreenWidth
        {
            get
            {
                if (DepthFormat == DepthImageFormat.Resolution80x60Fps30) return 80;
                if (DepthFormat == DepthImageFormat.Resolution320x240Fps30) return 320;
                if (DepthFormat == DepthImageFormat.Resolution640x480Fps30) return 640;
                return -1;
            }
        }

        public int ScreenHeight
        {
            get
            {
                if (DepthFormat == DepthImageFormat.Resolution80x60Fps30) return 60;
                if (DepthFormat == DepthImageFormat.Resolution320x240Fps30) return 240;
                if (DepthFormat == DepthImageFormat.Resolution640x480Fps30) return 480;
                return -1;
            }
        }

        private void SetDefault()
        {
            // Resolution of the depth image
            DepthFormat = DepthImageFormat.Resolution320x240Fps30;
            //depthFormat = DepthImageFormat.Resolution640x480Fps30;

            // Margins applied to the original size image (Percentaje)
            MarginLeftPerc = 0;
            MarginRightPerc = 0;
            MarginTopPerc = 0;
            MarginBotPerc = 0;

            // Percentage of valid space after the closest point, based on the furthest point
            NearSpacePerc = 0.05f;

            // Size of the valid space after the closest point
            AbsoluteSpace = -1; // 700 optimal

            // Size of the jumps of the contour and inside points used in the calculation of the center
            FindCenterContourJump = 15;
            FindCenterInsideJump = 15;

            // Size of the jump after check a possible fingertip
            FindFingertipsJump = 2;

            // Size of the jump after find a valid fingertips (Percentage over the total)
            FingertipFindJumpPerc = 0.10f; // Size of the jump

            // Variables used in the K-curvature algorithm for the calcultion of the fingertips
            Theta = 40*(Math.PI/180); // In radians
            K = 22;

            // Maximum number of tracked hands or figures
            MaxTrackedHands = 2;

            // Erode and Dilation iterations
            SmoothingIterations = 1;

            // Container box reduction
            ContainerBoxReduction = 0.5f;
        }
    }
}