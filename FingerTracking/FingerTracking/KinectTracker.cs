﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Kinect;

namespace FingerTracking
{
    public class KinectTracker
    {
        public delegate void AfterReady();


        private readonly bool connected;
        private readonly KinectSensor sensor;

        private AfterReady afterColorReady;
        private AfterReady afterDepthReady;
        public Bitmap ColorImage { get; private set; }
        private IntPtr colorPtr;
        public Bitmap DepthImage { get; private set; }
        private IntPtr depthPtr;

        public KinectTracker()
        {
            afterColorReady = skip;
            afterDepthReady = skip;

            Settings = new KinectSettings();

            Hands = new List<Hand>();

            // Check if there is any Kinect device connected
            if (KinectSensor.KinectSensors.Count > 0)
            {
                connected = true;
                sensor = KinectSensor.KinectSensors.ElementAt(0);

                //sensor.DepthStream.Range = DepthRange.Near;

                sensor.DepthFrameReady += DepthFrameReady;
                sensor.ColorFrameReady += ColorFrameReady;
            }
            else // No device connected
            {
                connected = false;
            }
        }

        public KinectSettings Settings { get; set; }

        public List<Hand> Hands { get; set; }

        private void skip()
        {
        }

        public void Start()
        {
            sensor.DepthStream.Enable(Settings.DepthFormat);
            sensor.ColorStream.Enable();
            sensor.Start();
        }

        public void Stop()
        {
            sensor.DepthStream.Disable();
            sensor.ColorStream.Disable();
            sensor.Stop();
        }

        public void SetEventColorReady(AfterReady del)
        {
            afterColorReady = del;
        }

        public void ClearEventColorReady()
        {
            afterColorReady = skip;
        }

        public void SetEventDepthReady(AfterReady del)
        {
            afterDepthReady = del;
        }

        public void ClearEventDepthReady()
        {
            afterDepthReady = skip;
        }

        public void ColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            var frame = e.OpenColorImageFrame();

            if (frame == null)
                return;

            var pixels = new byte[frame.PixelDataLength];
            frame.CopyPixelDataTo(pixels);

            Marshal.FreeHGlobal(colorPtr);
            colorPtr = Marshal.AllocHGlobal(pixels.Length);
            Marshal.Copy(pixels, 0, colorPtr, pixels.Length);

            int stride = frame.Width * 4;

            ColorImage = new Bitmap(frame.Width, frame.Height, stride, PixelFormat.Format32bppRgb, colorPtr);

            afterColorReady();
        }

        public void DepthFrameReady(object sender, DepthImageFrameReadyEventArgs e)
        {
            // Get the depth frame from Kinect
            DepthImageFrame frame = e.OpenDepthImageFrame();

            // Check that the frame is not null
            if (frame == null)
                return;

            // Calculate the real distance for every pixel in the depth image
            int[] distances = generateDistances(frame);

            // Return a 0 or 1 matrix, which contains wich pixels are near enough
            bool[][] near = GenerateValidMatrix(frame, distances);

            // Return the tracked hands based on the near pixels
            Hands = LocalizeHands(near);

            var pixels = new byte[frame.PixelDataLength * 4];

            // Free last depth Matrix
            Marshal.FreeHGlobal(depthPtr);
            depthPtr = Marshal.AllocHGlobal(pixels.Length);
            Marshal.Copy(pixels, 0, depthPtr, pixels.Length);

            // Create the bitmap
            int height = near.Length;
            int width = 0;
            if (near.Length > 0)
            {
                width = near[0].Length;
            }
            int stride = width * 4;

            DepthImage = new Bitmap(width, height, stride, PixelFormat.Format32bppRgb,
                                    depthPtr);

            // Calculate 3D points for the hands
            for (int i = 0; i < Hands.Count; ++i)
            {
                Hands[i].Calculate3DPoints(Settings.ScreenWidth, Settings.ScreenHeight,
                                           distances);
            }

            // Call the rest of the functions
            afterDepthReady();

            // Draw fingertips and palm center
            Graphics gBmp = Graphics.FromImage(DepthImage);
            Brush blueBrush = new SolidBrush(Color.Blue);
            Brush redBrush = new SolidBrush(Color.Red);

            for (int i = 0; i < Hands.Count; ++i)
            {
                // Red point which is the center of the palm
                gBmp.FillEllipse(redBrush, Hands[i].Palm.Y - 5, Hands[i].Palm.X - 5, 10,
                                 10);

                // Draw inside points
                //for (int j = 0; j < hands[i].inside.Length; ++j)
                //{
                //    Point p = hands[i].inside.Get(j);
                //    depthImage.SetPixel(p.Y, p.X, Color.Blue);
                //}

                // Draw the contour of the hands
                for (int j = 0; j < Hands[i].Contour.Count; ++j)
                {
                    PointFT p = Hands[i].Contour[j];
                    DepthImage.SetPixel(p.Y, p.X, Color.Red);
                }

                // Blue points which represent the fingertips
                for (int j = 0; j < Hands[i].Fingertips.Count; ++j)
                {
                    if (Hands[i].Fingertips[j].X != -1)
                    {
                        gBmp.FillEllipse(blueBrush, Hands[i].Fingertips[j].Y - 5,
                                         Hands[i].Fingertips[j].X - 5, 10, 10);
                    }
                }
            }

            blueBrush.Dispose();
            redBrush.Dispose();
            gBmp.Dispose();
        }

        private int[] generateDistances(DepthImageFrame frame)
        {
            // Raw depth data form the Kinect
            var depth = new short[frame.PixelDataLength];
            frame.CopyPixelDataTo(depth);

            // Calculate the real distance
            var distance = new int[frame.PixelDataLength];
            for (int i = 0; i < distance.Length; ++i)
            {
                distance[i] = depth[i] >> DepthImageFrame.PlayerIndexBitmaskWidth;
            }

            return distance;
        }

        private bool[][] GenerateValidMatrix(DepthImageFrame frame, int[] distance)
        {
            // Create the matrix. The size depends on the margins
            var x1 = (int)(frame.Width * Settings.MarginLeftPerc / 100.0f);
            var x2 = (int)(frame.Width * (1 - (Settings.MarginRightPerc / 100.0f)));
            var y1 = (int)(frame.Height * Settings.MarginTopPerc / 100.0f);
            var y2 = (int)(frame.Height * (1 - (Settings.MarginBotPerc / 100.0f)));
            var near = new bool[y2 - y1][];
            for (int i = 0; i < near.Length; ++i)
            {
                near[i] = new bool[x2 - x1];
            }

            // Calculate max and min distance
            int max = int.MinValue, min = int.MaxValue;

            for (int k = 0; k < distance.Length; ++k)
            {
                if (distance[k] > max) max = distance[k];
                if (distance[k] < min && distance[k] != -1) min = distance[k];
            }

            // Decide if it is near or not
            var margin = (int)(min + Settings.NearSpacePerc * (max - min));
            int index = 0;
            if (Settings.AbsoluteSpace != -1) margin = min + Settings.AbsoluteSpace;
            for (int i = 0; i < near.Length; ++i)
            {
                for (int j = 0; j < near[i].Length; ++j)
                {
                    index = frame.Width * (i + y1) + (j + x1);
                    if (distance[index] <= margin && distance[index] != -1)
                    {
                        near[i][j] = true;
                    }
                    else
                    {
                        near[i][j] = false;
                    }
                }
            }

            // Dilate and erode the image to get smoother figures
            if (Settings.SmoothingIterations > 0)
            {
                near = Dilate(near, Settings.SmoothingIterations);
                near = Erode(near, Settings.SmoothingIterations);
            }

            // Mark as not valid the borders of the matrix to improve the efficiency in some methods
            // First row
            for (int j = 0; j < near[0].Length; ++j)
                near[0][j] = false;

            // Last row
            var m = near.Length - 1;
            for (int j = 0; j < near[0].Length; ++j)
                near[m][j] = false;

            // First column
            for (int i = 0; i < near.Length; ++i)
                near[i][0] = false;

            // Last column
            m = near[0].Length - 1;
            foreach (bool[] t in near)
                t[m] = false;

            return near;
        }

        private List<Hand> LocalizeHands(bool[][] valid)
        {
            int i, j, k;

            var hands = new List<Hand>();

            var insidePoints = new List<PointFT>();
            var contourPoints = new List<PointFT>();
            
            var contour = new bool[valid.Length][];
            for (i = 0; i < valid.Length; ++i)
            {
                contour[i] = new bool[valid[0].Length];
            }

            // Divide points in contour and inside points
            for (i = 1; i < valid.Length - 1; ++i)
            {
                for (j = 1; j < valid[i].Length - 1; ++j)
                {
                    if (valid[i][j])
                    {
                        // Count the number of valid adjacent points
                        var count = numValidPixelAdjacent(ref i, ref j, ref valid); 

                        if (count == 4) // Inside
                        {
                            insidePoints.Add(new PointFT(i, j));
                        }
                        else // Contour
                        {
                            contour[i][j] = true;
                            contourPoints.Add(new PointFT(i, j));
                        }
                    }
                }
            }

            // Create the sorted contour list, using the turtle algorithm
            for (i = 0; i < contourPoints.Count; ++i)
            {
                var hand = new Hand();

                // If it is a possible start point
                if (contour[contourPoints[i].X][contourPoints[i].Y])
                {
                    // Calculate the contour
                    hand.Contour = CalculateFrontier(ref valid, contourPoints[i], ref contour);

                    // Check if the contour is big enough to be a hand
                    if (hand.Contour.Count / (contourPoints.Count * 1.0f) > 0.20f && hand.Contour.Count > Settings.K)
                    {
                        // Calculate the container box
                        hand.CalculateContainerBox(Settings.ContainerBoxReduction);

                        // Add the hand to the list
                        hands.Add(hand);
                    }

                    // Don't look for more hands, if we reach the limit
                    if (hands.Count >= Settings.MaxTrackedHands)
                    {
                        break;
                    }
                }
            }

            // Allocate the inside points to the correct hand using its container box

            //List<int> belongingHands = new List<int>();
            for (i = 0; i < insidePoints.Count; ++i)
            {
                for (j = 0; j < hands.Count; ++j)
                {
                    if (hands[j].IsPointInsideContainerBox(insidePoints[i]))
                    {
                        hands[j].Inside.Add(insidePoints[i]);
                        //belongingHands.Add(j);
                    }
                }

                // A point can only belong to one hand, if not we don't take that point into account
                /*if (belongingHands.Count == 1)
                {
                    hands[belongingHands.ElementAt(0)].inside.Add(insidePoints[i]);
                }
                belongingHands.Clear();*/
            }

            // Find the center of the palm
            float max;

            for (i = 0; i < hands.Count; ++i)
            {
                max = float.MinValue;
                for (j = 0; j < hands[i].Inside.Count; j += Settings.FindCenterInsideJump)
                {
                    var min = float.MaxValue;
                    for (k = 0; k < hands[i].Contour.Count; k += Settings.FindCenterInsideJump)
                    {
                        var distance = PointFT.distanceEuclidean(hands[i].Inside[j], hands[i].Contour[k]);

                        if (!hands[i].IsCircleInsideContainerBox(hands[i].Inside[j], distance)) 
                            continue;

                        if (distance < min) 
                            min = distance;

                        if (min < max) 
                            break;
                    }

                    if (max < min && min != float.MaxValue)
                    {
                        max = min;
                        hands[i].Palm = hands[i].Inside[j];
                    }
                }
            }

            // Find the fingertips
            PointFT p1, p2, p3, pAux, r1, r2;
            int size;
            double angle;
            int jump;

            for (i = 0; i < hands.Count; ++i)
            {
                // Check if there is a point at the beginning to avoid checking the last ones of the list
                max = hands[i].Contour.Count;
                size = hands[i].Contour.Count;
                jump = (int)(size * Settings.FingertipFindJumpPerc);
                for (j = 0; j < Settings.K; j += 1)
                {
                    p1 = hands[i].Contour[(j - Settings.K + size) % size];
                    p2 = hands[i].Contour[j];
                    p3 = hands[i].Contour[(j + Settings.K) % size];
                    r1 = p1 - p2;
                    r2 = p3 - p2;

                    angle = PointFT.angle(r1, r2);

                    if (angle > 0 && angle < Settings.Theta)
                    {
                        pAux = p3 + ((p1 - p3) / 2);
                        if (PointFT.distanceEuclideanSquared(pAux, hands[i].Palm) >
                            PointFT.distanceEuclideanSquared(hands[i].Contour[j], hands[i].Palm))
                            continue;

                        hands[i].Fingertips.Add(hands[i].Contour[j]);
                        max = hands[i].Contour.Count + j - jump;
                        max = Math.Min(max, hands[i].Contour.Count);
                        j += jump;
                        break;
                    }
                }

                // Continue with the rest of the points
                for (; j < max; j += Settings.FindFingertipsJump)
                {
                    p1 = hands[i].Contour[(j - Settings.K + size) % size];
                    p2 = hands[i].Contour[j];
                    p3 = hands[i].Contour[(j + Settings.K) % size];
                    r1 = p1 - p2;
                    r2 = p3 - p2;

                    angle = PointFT.angle(r1, r2);

                    if (angle > 0 && angle < Settings.Theta)
                    {
                        pAux = p3 + ((p1 - p3) / 2);
                        if (PointFT.distanceEuclideanSquared(pAux, hands[i].Palm) >
                            PointFT.distanceEuclideanSquared(hands[i].Contour[j], hands[i].Palm))
                            continue;

                        hands[i].Fingertips.Add(hands[i].Contour[j]);
                        j += jump;
                    }
                }
            }

            return hands;
        }

        /*
         * This function calcute the border of a closed figure starting in one of the contour points.
         * The turtle algorithm is used.
         */

        private List<PointFT> CalculateFrontier(ref bool[][] valid, PointFT start, ref bool[][] contour)
        {
            var list = new List<PointFT>();
            var last = new PointFT(-1, -1);
            var current = new PointFT(start);
            int dir = 0;

            do
            {
                if (valid[current.X][current.Y])
                {
                    dir = (dir + 1) % 4;
                    if (current != last)
                    {
                        list.Add(new PointFT(current.X, current.Y));
                        last = new PointFT(current);
                        contour[current.X][current.Y] = false;
                    }
                }
                else
                {
                    dir = (dir + 4 - 1) % 4;
                }

                switch (dir)
                {
                    case 0:
                        current.X += 1;
                        break; // Down
                    case 1:
                        current.Y += 1;
                        break; // Right
                    case 2:
                        current.X -= 1;
                        break; // Up
                    case 3:
                        current.Y -= 1;
                        break; // Left
                }
            } while (current != start);

            return list;
        }

        private bool[][] Dilate(bool[][] image, int it)
        {
            // Matrix to store the dilated image
            var dilateImage = new bool[image.Length][];
            for (int i = 0; i < image.Length; ++i)
            {
                dilateImage[i] = new bool[image[i].Length];
            }

            // Distances matrix
            int[][] distance = manhattanDistanceMatrix(image, true);

            // Dilate the image
            for (int i = 0; i < image.Length; i++)
            {
                for (int j = 0; j < image[i].Length; j++)
                {
                    dilateImage[i][j] = ((distance[i][j] <= it) ? true : false);
                }
            }

            return dilateImage;
        }

        private bool[][] Erode(bool[][] image, int it)
        {
            // Matrix to store the dilated image
            var erodeImage = new bool[image.Length][];
            for (int i = 0; i < image.Length; ++i)
            {
                erodeImage[i] = new bool[image[i].Length];
            }

            // Distances matrix
            int[][] distance = manhattanDistanceMatrix(image, false);

            // Dilate the image
            for (int i = 0; i < image.Length; i++)
            {
                for (int j = 0; j < image[i].Length; j++)
                {
                    erodeImage[i][j] = ((distance[i][j] > it) ? true : false);
                }
            }

            return erodeImage;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="zeroDistanceValue"></param>
        /// <returns></returns>
        private int[][] manhattanDistanceMatrix(bool[][] image, bool zeroDistanceValue)
        {
            var distanceMatrix = new int[image.Length][];
            for (int i = 0; i < distanceMatrix.Length; ++i)
            {
                distanceMatrix[i] = new int[image[i].Length];
            }

            // traverse from top left to bottom right
            for (int i = 0; i < distanceMatrix.Length; i++)
            {
                for (int j = 0; j < distanceMatrix[i].Length; j++)
                {
                    if ((image[i][j] && zeroDistanceValue) || (!image[i][j] && !zeroDistanceValue))
                    {
                        // first pass and pixel was on, it gets a zero
                        distanceMatrix[i][j] = 0;
                    }
                    else
                    {
                        // pixel was off
                        // It is at most the sum of the lengths of the array
                        // away from a pixel that is on
                        distanceMatrix[i][j] = image.Length + image[i].Length;
                        // or one more than the pixel to the north
                        if (i > 0) distanceMatrix[i][j] = Math.Min(distanceMatrix[i][j], distanceMatrix[i - 1][j] + 1);
                        // or one more than the pixel to the west
                        if (j > 0) distanceMatrix[i][j] = Math.Min(distanceMatrix[i][j], distanceMatrix[i][j - 1] + 1);
                    }
                }
            }
            // traverse from bottom right to top left
            for (int i = distanceMatrix.Length - 1; i >= 0; i--)
            {
                for (int j = distanceMatrix[i].Length - 1; j >= 0; j--)
                {
                    // either what we had on the first pass
                    // or one more than the pixel to the south
                    if (i + 1 < distanceMatrix.Length)
                        distanceMatrix[i][j] = Math.Min(distanceMatrix[i][j], distanceMatrix[i + 1][j] + 1);
                    // or one more than the pixel to the east
                    if (j + 1 < distanceMatrix[i].Length)
                        distanceMatrix[i][j] = Math.Min(distanceMatrix[i][j], distanceMatrix[i][j + 1] + 1);
                }
            }

            return distanceMatrix;
        }

        /*
         * Counts the number of adjacent valid points without taking into account the diagonals
         */

        private int numValidPixelAdjacent(ref int i, ref int j, ref bool[][] valid)
        {
            int count = 0;

            if (valid[i + 1][j]) ++count;
            if (valid[i - 1][j]) ++count;
            if (valid[i][j + 1]) ++count;
            if (valid[i][j - 1]) ++count;
            //if (valid[i + 1][j + 1]) ++count;
            //if (valid[i + 1][j - 1]) ++count;
            //if (valid[i - 1][j + 1]) ++count;
            //if (valid[i - 1][j - 1]) ++count;

            return count;
        }

        // Generate a representable image of the valid matrix
        private byte[] GenerateDepthImage(bool[][] near)
        {
            // Image pixels
            var pixels = new byte[near.Length * near[0].Length * 4];
            int width = near[0].Length;

            for (int i = 1; i < near.Length - 1; ++i)
            {
                for (int j = 1; j < near[i].Length - 1; ++j)
                {
                    if (near[i][j])
                    {
                        if (!near[i + 1][j] || !near[i - 1][j]
                            || !near[i][j + 1] || !near[i][j - 1]) // Is border
                        {
                            pixels[(i * width + j) * 4 + 0] = 255;
                            pixels[(i * width + j) * 4 + 1] = 0;
                            pixels[(i * width + j) * 4 + 2] = 0;
                            pixels[(i * width + j) * 4 + 3] = 0;
                        }
                        else
                        {
                            pixels[(i * width + j) * 4 + 0] = 255;
                            pixels[(i * width + j) * 4 + 1] = 255;
                            pixels[(i * width + j) * 4 + 2] = 255;
                            pixels[(i * width + j) * 4 + 3] = 0;
                        }
                    }
                }
            }

            return pixels;
        }

        public bool isConnected()
        {
            return connected;
        }

        public Bitmap getDepthImage()
        {
            return DepthImage;
        }

        public Bitmap getColorImage()
        {
            return ColorImage;
        }
    }
}